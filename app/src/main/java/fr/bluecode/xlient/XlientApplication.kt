package fr.bluecode.xlient

import android.app.Application
import fr.bluecode.xlient.data.cache.initializeCache

class XlientApplication : Application()
{
    override fun onCreate()
    {
        super.onCreate()
        initializeCache(this)
    }
}