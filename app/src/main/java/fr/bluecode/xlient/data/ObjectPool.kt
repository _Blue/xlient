@file:Suppress("UNCHECKED_CAST")

package fr.bluecode.xlient.data

import android.util.SparseArray
import androidx.core.util.containsKey
import androidx.core.util.set
import fr.bluecode.xlient.api.model.DataModel

object ObjectPool
{
    private val objects = SparseArray<DataModel>()
    fun <T> get(id: Int): T? where T: DataModel
    {
        return if (objects.containsKey(id))
        {
            objects[id] as T
        }
        else
        {
            null
        }
    }

    fun store(value: DataModel)
    {
        objects[value.id] = value
    }
}