package fr.bluecode.xlient.data

import android.content.Context
import fr.bluecode.xlient.api.Client
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.data.cache.DatabaseHelper
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.loadConfiguration

class DataViewFactory
{
    fun createView(context: Context, config: Configuration = loadConfiguration(context)): APIDataView
    {
        return APIDataView(Client(config, ObjectPool), ObjectCache(config, DatabaseHelper, ObjectPool))
    }
}