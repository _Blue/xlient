package fr.bluecode.xlient.data

import android.util.Log
import fr.bluecode.xlient.exceptions.ErrorFoundOnBackendException

object ErrorPool
{
    private val errors = mutableMapOf<Int, MutableList<Exception>>()

    private var counter = 0;

    fun createContext(): Int
    {
        errors[counter] = mutableListOf()

        counter++

        return counter - 1
    }

    fun freeContext(context: Int)
    {
        errors.remove(context)
    }

    fun add(context: Int, error: Exception)
    {
        if (!errors.containsKey(context))
        {
            Log.e("Error", "Error context $context not found, dropping error $error")
            return
        }


        if (error is ErrorFoundOnBackendException)
        {
            if (errors[context]!!.any{it is ErrorFoundOnBackendException && it.error.id == error.error.id})
            {
                return // Error already logged, drop
            }
        }

        errors[context]!!.add(error)
    }

    fun get(context: Int): MutableList<Exception>
    {
        if (!errors.containsKey(context))
        {
            errors[context] = mutableListOf()
        }

        return errors[context]!!
    }
}