package fr.bluecode.xlient.data

import android.util.Log
import com.github.kittinunf.fuel.core.ProgressCallback
import fr.bluecode.xlient.api.Client
import fr.bluecode.xlient.api.model.*
import fr.bluecode.xlient.data.cache.ObjectCache
import java.io.InputStream
import java.util.*

class APIDataView(val client: Client, val cache: ObjectCache) : DataView
{
    override fun getConversations(
        success: (result: List<Conversation>, done: Boolean) -> Unit,
        failure: (error: Exception) -> Unit
    )
    {
        val readObjects = mutableSetOf<Int>()

        // The latestConversation needs to be fetched separately because last_updated doesn't necessarily
        // match when received the last message
        val latestConversation = cache.getMostRecent<Conversation>("last_updated", readObjects)

        val forward = latestConversation != null

        if (forward)
        {
            val cached = cache.lookup<Conversation>(client.configuration.conversationToShow, null, readObjects)
            success(cached, false)
        }

        // Two branches:
        // - The cache is completely empty, and we'll fetch all conversations from the endpoint
        // - The cache is not empty and we'll only fetch the diff

        val startPoint = if (forward) listOf(latestConversation!!) else listOf()
        client.fetchObjects(
            { it: List<Conversation> -> onConversationsFetched(it, success, failure, forward)},
            failure,
            arguments = conversationIterationArguments(startPoint, forward, true)
        )
        Log.i("DataView", "Scheduled conversation lookup request")
    }

    fun getConversations(
        updated_at: Date,
        order: String,
        success: (result: List<Conversation>) -> Unit,
        failure: (error: Exception) -> Unit
    )
    {
        val ts = updated_at.time.toBigDecimal().divide(1000.toBigDecimal()).plus(0.001.toBigDecimal())

        return client.fetchObjects<Conversation>(
            {  success(it)},
            failure,
            arguments = mapOf("updated_after" to ts.toString(), "order" to order)
        )
    }

    private fun conversationIterationArguments(conversations: List<Conversation>, forward: Boolean, initial: Boolean): Map<String, String>
    {
        return if (forward)
        {
            val newest = conversations.maxBy { it.last_updated }!!.last_updated.time.toBigDecimal()

            var updated_after = newest.divide(1000.toBigDecimal())

            // Trick: The backend updates conversations once a day at the same time, which tends to create a 'pack'
            // of conversation with equal X.last_updated, which leads to us always downloading the same pack of updates.
            // Adding 1 ms fixes the problem, and in case a conversation actually received a message 1 ms after being updated,
            // That updated would be fetched as part of the message diff sync, so no message can be lost here
            if (initial)
            {
                updated_after = updated_after.plus(0.001.toBigDecimal())
            }

            mapOf("updated_after" to updated_after.toString(), "order" to "asc")
        }
        else
        {
            val oldest = conversations.minBy { it.last_updated }?.last_updated?.time?.toBigDecimal()

            val arguments = mutableMapOf("order" to "desc")
            if (oldest != null) // Oldest is null on the first iteration
            {
                arguments["updated_after"] = oldest.divide(1000.toBigDecimal()).toString()
            }
            return arguments
        }
    }

    private fun onConversationsFetched(
        conversations: List<Conversation>,
        success: (result: List<Conversation>, done: Boolean) -> Unit,
        failure: (error: Exception) -> Unit,
        forward: Boolean)
    {
        if (conversations.count() < client.configuration.conversationsPerFetch)
        {
            success(conversations, true)
        }
        else
        {
            success(conversations, false)

            client.fetchObjects(
                { it: List<Conversation> -> onConversationsFetched(it, success, failure, forward)},
                failure,
                arguments = conversationIterationArguments(conversations, forward, false)
            )
        }

        cache(conversations)
    }

    fun getOldConversations(olderThan: Date) : List<Conversation>
    {
        return cache.lookup<Conversation>(max = 0, updatedBefore = olderThan)
    }

    fun cache(objects: List<DataModel>)
    {
        val cached = mutableSetOf<Int>()
        for (e in objects)
        {
            cache.cache(e, cached)
        }
    }

    override fun getMessages(
        success: (result: List<Message>) -> Unit,
        failure: (error: Exception) -> Unit,
        conversation: Conversation?,
        from_id: Int?,
        order: String?,
        count: Int?
    )
    {
        val route =
            if (conversation != null) "conversation/${conversation.id}/messages" else "messages"
        val args = mutableMapOf<String, String>()

        if (from_id != null)
        {
            args["from_id"] = from_id.toString()
        }

        if (order != null)
        {
            args["order"] = order.toString()
        }

        if (count != null)
        {
            args["count"] = count.toString()
        }

        client.fetchObjects({ it: List<Message> ->
            success(it); if (from_id == null || conversation == null) cacheMessages(
            it
        )
        }, failure, route, args)
    }

    private fun cacheMessages(messages: List<Message>)
    {
        cache(messages.sortedBy { it.timestamp })
    }

    override fun markAsRead(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        message: Message,
        updateBackend: Boolean
    )
    {
        val body = mapOf<String, Any?>(
            "update_backend" to updateBackend
        )

        client.post<Message>(
            { success(it); cache.cache(it) },
            failure,
            "message/${message.id}/read",
            body
        )
    }

    override fun sendMessage(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        conversation: Conversation,
        text: String,
        attachment: List<UploadedAttachment>?,
        reply_to: Message?
    )
    {
        val body = mutableMapOf<String, Any?>()
        body["text"] = text

        if (attachment != null)
        {
            body["attachments"] = attachment.map { it.id }
        }

        if (reply_to != null)
        {
            body["reply_to"] = reply_to.id
        }

        client.post<Message>(
            success,
            failure,
            "conversation/${conversation.id}/message",
            body
        )
    }

    override fun uploadAttachment(
        success: (result: UploadedAttachment) -> Unit,
        failure: (error: Exception) -> Unit,
        fileName: String,
        content: InputStream,
        progress: ProgressCallback
    )
    {
        client.post<UploadedAttachment>(success, failure, "upload_attachment/$fileName", content.readBytes(), progress = progress)
    }

    override fun reactToMessage(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        message: Message,
        reaction: Map<String, Any?>
    )
    {
        client.post<Message>({success(it); cache.update(it)}, failure, "message/${message.id}/react", reaction)
    }

    inline fun <reified T> fetchObject(
        crossinline success: (result: T) -> Unit,
        crossinline failure: (error: Exception) -> Unit,
        id: Int) where T: DataModel
    {
        client.fetchObject<T>({success(it); cache.update(it); }, failure, id)
    }

    fun search(
        success: (result: List<Message>) -> Unit,
        failure: (error: Exception) -> Unit,
        query: String,
        from: Int?,
        count: Int?,
        conversation: Conversation?,
        author: Contact?
    )
    {
        val route =
            if (conversation == null) "search" else "conversation/${conversation.id}/message/search"

        val arguments = mutableMapOf("text" to query)
        if (from != null)
        {
            arguments["from"] = from.toString()
        }

        if (count != null)
        {
            arguments["count"] = count.toString()
        }

        if (author != null)
        {
            arguments["author"] = author.id.toString()
        }

        return client.fetchObjects(success, failure, route, arguments)
    }
}