package fr.bluecode.xlient.data.cache

import fr.bluecode.xlient.api.Deserializer
import fr.bluecode.xlient.api.PostAssign
import fr.bluecode.xlient.api.TypeInformation
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.data.ObjectPool

val referenceFieldKeys = setOf("id", "type")

class CacheDeserializer(private val objectPool: ObjectPool, private val objectCache: ObjectCache): Deserializer(objectPool)
{
    override fun constructImpl(
        type: TypeInformation,
        content: Map<String, com.jsoniter.any.Any>,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>): Any?
    {
        val id = content["id"]!!.toInt()

        if (content.keys == referenceFieldKeys) // Cached object
        {
            val cached = objectPool.get<DataModel>(id)
            if (cached != null)
            {
                return cached
            }

            if (!readObjects.contains(id)) // Break potential infinite recursion here
            {
                return objectCache.lookupId(id, path, readObjects, postAssign)
            }
        }

        return super.constructImpl(type, content, path, readObjects, postAssign)
    }
}