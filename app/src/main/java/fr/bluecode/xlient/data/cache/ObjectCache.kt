package fr.bluecode.xlient.data.cache

import android.util.Log
import com.fasterxml.jackson.databind.util.ISO8601Utils
import com.jsoniter.JsonIterator
import com.jsoniter.output.JsonStream
import fr.bluecode.xlient.api.Lazy
import fr.bluecode.xlient.api.PostAssign
import fr.bluecode.xlient.api.TypeInformation
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.api.model.Facebook.FacebookConversation
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.api.types
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.data.ObjectPool
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import java.lang.RuntimeException
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.superclasses

class ObjectCache(
    private val configuration: Configuration,
    val database: DatabaseHelper,
    objectPool: ObjectPool
)
{
    private val deserializer = CacheDeserializer(objectPool, this)

    fun cache(dataModel: DataModel, cachedObjects: MutableSet<Int> = mutableSetOf())
    {
        val modelType = getModelType(dataModel)

        val types = modelType.typeString.split("/")
        database.storeObjectContent(
                            dataModel.id,
                            JsonStream.serialize(serialize(dataModel, modelType, cachedObjects)),
                            dataModel.last_updated,
                            getLastUpdatedChild(dataModel),
                            types)
    }

    fun update(dataModel: DataModel, cachedObjects: MutableSet<Int> = mutableSetOf()): Int
    {
        return database.updateObjectContent(
            dataModel.id,
            JsonStream.serialize(serialize(dataModel, getModelType(dataModel), cachedObjects)),
            dataModel.last_updated,
            getLastUpdatedChild(dataModel))
    }

    private fun getModelType(dataModel: DataModel): TypeInformation
    {
        val modelType = types[dataModel.javaClass.simpleName]
        if (modelType == null)
        {
            throw RuntimeException("Unexpected type to serialize: ${dataModel.javaClass.name}")
        }

        return modelType
    }

    private fun getLastUpdatedChild(dataModel: DataModel): Date
    {
        return when(dataModel)
        {
            is Conversation -> dataModel.last_message?.timestamp ?: dataModel.last_updated
            else -> dataModel.last_updated
        }
    }

    fun lookupId(id: Int, path: String = "/", readObjects: MutableSet<Int> = mutableSetOf(), postAssign: MutableList<PostAssign> = mutableListOf()) : DataModel?
    {
        val content = database.getObjectContent(id)
        return if (content == null)
        {
            null
        }
        else
        {
            return deserialize(JsonIterator.deserialize(content), path, readObjects, postAssign)
        }
    }

    fun deserialize(content: com.jsoniter.any.Any, path: String, readObjects: MutableSet<Int>, postAssign: MutableList<PostAssign>): DataModel?
    {
        return if (path.isEmpty())
        {
            deserializer.deserializeJson(content, path, readObjects) as DataModel?
        }
        else
        {
            deserializer.deserializeJsonImpl(content, path, readObjects, postAssign) as DataModel?
        }
    }

    fun getSortingField(type: KClass<*>): String
    {
        val superClasses = type.allSuperclasses + setOf(type)
        return if (superClasses.contains(Conversation::class) || superClasses.contains(Message::class))
        {
            "last_updated_child"
        }
        else
        {
            "last_updated"
        }
    }

    inline fun <reified T> deserialize(content: List<String>, readObjects: MutableSet<Int> = mutableSetOf()): List<T>
    {
        val ts = System.currentTimeMillis()
        val childrenJson = runBlocking { content.map { GlobalScope.async{JsonIterator.deserialize(it)} }.awaitAll()}

        val parsedTs = System.currentTimeMillis()

        Log.i("ObjectCache", "Parsed ${childrenJson.count()} objects of type ${T::class.simpleName} in ${parsedTs - ts} ms")

        val output =  childrenJson.map { deserialize(it, "", readObjects, mutableListOf()) as T }

        Log.i("ObjectCache", "Built ${childrenJson.count()} objects of type ${T::class.simpleName} in ${System.currentTimeMillis() - parsedTs} ms")

        return output
    }

    inline fun <reified T> lookup(max: Int = 0, updatedBefore: Date? = null, readObjects: MutableSet<Int> = mutableSetOf()): List<T> where T: DataModel
    {
        val content = database.getObjects(T::class.simpleName!!, updatedBefore, getSortingField(T::class))

        return deserialize(if (max == 0) content else content.take(max), readObjects)
    }

    inline fun <reified T> getMostRecent(sortingField: String, readObjects: MutableSet<Int>): T?
    {
        val content = database.getMostRecent(T::class.simpleName!!, sortingField);
        if (content == null)
        {
            return null
        }

        return deserialize(JsonIterator.deserialize(content), "", readObjects, mutableListOf()) as T
    }

    private fun serialize(
        dataModel: DataModel,
        modelType: TypeInformation,
        cachedObjects: MutableSet<Int>
    ): Map<String, Any?>
    {
        val output = mutableMapOf<String, Any?>()
        for (e in modelType.fields)
        {
            output[e.key] = serializeMember(e.value.get(dataModel), cachedObjects)
        }

        output["type"] = modelType.typeString

        return output
    }

    private fun serializeList(value: List<*>, cachedObjects: MutableSet<Int>): List<Any?>
    {
        return value.map { serializeMember(it, cachedObjects) }
    }

    private fun serializeMember(member: Any?, cachedObjects: MutableSet<Int>): Any?
    {
        return when (member)
        {
            null -> null
            is DataModel -> serializeInnerObject(member, cachedObjects)
            is Date -> ISO8601Utils.format(member, true)
            is List<*> -> serializeList(member, cachedObjects)
            is Lazy<*> -> serializeMember(member.get(), cachedObjects)
            else -> member
        }
    }

    private fun serializeInnerObject(value: DataModel, cachedObjects: MutableSet<Int>): Any
    {
        if (!cachedObjects.contains(value.id)) // Optimization to avoid writing the same object multiple times
        {
            cachedObjects.add(value.id)
            cache(value, cachedObjects)
        }

        val output = mutableMapOf<String, Any?>()
        output["id"] = value.id
        output["type"] = getModelType(value).typeString

        return output
    }
}