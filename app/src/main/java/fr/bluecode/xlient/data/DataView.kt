package fr.bluecode.xlient.data

import com.github.kittinunf.fuel.core.ProgressCallback
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.api.model.UploadedAttachment
import java.io.InputStream

interface DataView
{
    fun getConversations(
        success: (result: List<Conversation>, done: Boolean) -> Unit,
        failure: (error: Exception) -> Unit
    )

    fun getMessages(
        success: (result: List<Message>) -> Unit,
        failure: (error: Exception) -> Unit,
        conversation: Conversation? = null,
        from_id: Int? = null,
        order: String? = null,
        count: Int? = null
    )

    fun sendMessage(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        conversation: Conversation,
        text: String,
        attachment: List<UploadedAttachment>?,
        reply_to: Message?
    )

    fun uploadAttachment(
        success: (result: UploadedAttachment) -> Unit,
        failure: (error: Exception) -> Unit,
        fileName: String,
        content: InputStream,
        progress: ProgressCallback
    )

    fun markAsRead(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        message: Message,
        updateBackend: Boolean
    )

    fun reactToMessage(
        success: (result: Message) -> Unit,
        failure: (error: Exception) -> Unit,
        message: Message,
        reaction: Map<String, Any?>
    )
}