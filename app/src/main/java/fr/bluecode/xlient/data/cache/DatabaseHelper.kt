package fr.bluecode.xlient.data.cache

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import fr.bluecode.xlient.BuildConfig
import java.time.Instant
import java.util.*

const val DATABASE_NAME = "xocial"
const val DATABASE_VERSION = BuildConfig.VERSION_CODE / 100 // ignore patch number (doesn't change data model)


const val OBJECT_TABLE = "object"

const val SQL_CREATE_OBJECT_TABLE = "CREATE TABLE $OBJECT_TABLE " +
                                            "(id INTEGER PRIMARY KEY," +
                                            " content TEXT," +
                                            " type TEXT," +
                                            " last_updated INTEGER," +
                                            " last_updated_child INTEGER" +
                                            ")"

const val SQL_DROP_TABLE = "DROP TABLE IF EXISTS $OBJECT_TABLE"

private lateinit var context: Context

fun initializeCache(ctx: Context)
{
    context = ctx
}

object DatabaseHelper: SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION)
{
    override fun onCreate(db: SQLiteDatabase)
    {
        synchronized(this)
        {
            db.execSQL(SQL_CREATE_OBJECT_TABLE)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int)
    {
        synchronized(this)
        {
            db.execSQL(SQL_DROP_TABLE) // Just drop cache in case of schema upgrade
            onCreate(db)
        }
    }

    fun getObjectContent(id: Int): String?
    {
        synchronized(this)
        {
            readableDatabase.use {

                it.query(OBJECT_TABLE,
                    null,
                    "id=?",
                    arrayOf(id.toString()),
                    null,
                    null,
                    null).use {

                    if (!it.moveToNext())
                    {
                        return null
                    }

                    return it.getString(1)
                }
            }
        }
    }

    fun storeObjectContent(id: Int, content: String, last_updated: Date, last_updated_child: Date?, types: List<String>)
    {
        synchronized(this)
        {
            writableDatabase.use {
                val columns = ContentValues()
                columns.put("id", id)
                columns.put("content", content)
                columns.put("last_updated", last_updated.time)
                if (last_updated_child != null)
                {
                    columns.put("last_updated_child", last_updated_child.time)
                }

                columns.put(
                    "type",
                    types.joinToString(prefix = "/", postfix = "/", separator = "/")
                )
                if (it.replace(OBJECT_TABLE, null, columns) < 0)
                {
                    throw RuntimeException("Failed to replace row for object $id, content: $content")
                }
            }
        }
    }

    fun updateObjectContent(id: Int, content: String, last_updated: Date, last_updated_child: Date?): Int
    {
        synchronized(this)
        {
            writableDatabase.use {
                val columns = ContentValues()
                columns.put("content", content)
                columns.put("last_updated", last_updated.time)
                if (last_updated_child != null)
                {
                    columns.put("last_updated_child", last_updated_child.time)
                }

                return it.update(
                    OBJECT_TABLE,
                    columns,
                    "id=?",
                    listOf(id.toString()).toTypedArray()
                )
            }
        }
    }

    fun getObjects(type: String, updatedBefore: Date?, last_updated_field: String): List<String>
    {
        val filter = if (updatedBefore != null) "AND $last_updated_field < ${updatedBefore.time}" else ""

        synchronized(this)
        {
            readableDatabase.use {
                it.query(
                    OBJECT_TABLE,
                    arrayOf("content"),
                    "instr(type, ?) > 0 $filter",
                    arrayOf("/$type/"),
                    null,
                    null,
                    "$last_updated_field DESC"
                ).use {

                    val output = mutableListOf<String>()

                    while (it.moveToNext())
                    {
                        output.add(it.getString(0))
                    }

                    return output
                }
            }
        }
    }

    fun getMostRecent(type: String, field: String): String?
    {
        synchronized(this)
        {
            readableDatabase.use {
                it.query(
                    OBJECT_TABLE,
                    arrayOf("content"),
                    "instr(type, ?) > 0",
                    arrayOf("/$type/"),
                    null,
                    null,
                    "$field DESC",
                    "1"
                    ).use {

                    return if (it.moveToNext())
                    {
                        it.getString(0)
                    }
                    else
                    {
                        null
                    }
                }
            }
        }
    }

    fun clear()
    {
        synchronized(this)
        {
            writableDatabase.use {
                it.execSQL(SQL_DROP_TABLE)
                onCreate(it)
            }
        }
    }
}