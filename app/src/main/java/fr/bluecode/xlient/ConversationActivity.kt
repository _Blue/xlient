package fr.bluecode.xlient

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.format.Formatter
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.Surface
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessagesListAdapter
import fr.bluecode.xlient.adapters.*
import fr.bluecode.xlient.api.model.*
import fr.bluecode.xlient.api.model.Facebook.FacebookImageAttachment
import fr.bluecode.xlient.api.model.Facebook.FacebookSource
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.api.model.Synthetic.SyntheticMessage
import fr.bluecode.xlient.background.IDataModelNotificationsHandler
import fr.bluecode.xlient.background.NotificationHandler
import fr.bluecode.xlient.exceptions.ErrorFoundOnBackendException
import fr.bluecode.xlient.data.*
import fr.bluecode.xlient.settings.loadConfiguration
import kotlinx.android.synthetic.main.activity_conversation.*
import kotlinx.android.synthetic.main.content_conversation.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log
import kotlin.math.max
import kotlin.reflect.jvm.internal.impl.descriptors.runtime.components.RuntimeErrorReporter


private const val REQUEST_TAKE_IMAGE = 1

private const val REQUEST_SELECT_FILE = 2

private const val REQUEST_CAMERA_PERMISSIONS = 3

private val CAMERA_PERMISSIONS = listOf(
    "android.permission.WRITE_EXTERNAL_STORAGE",
    "android.permission.ACCESS_FINE_LOCATION",
    "android.permission.READ_PHONE_STATE",
    "android.permission.CAMERA").toTypedArray()

enum class TaskType
{
    FETCH_OLD_MESSAGES,
    FETCH_NEW_MESSAGES,
    FETCH_CONVERSATION,
    SCROLL_TO_FIRST_UNREAD,
    REFRESH_UNSENT_MESSAGE
}

class ConversationActivity : SyncedActivity(), IDataModelNotificationsHandler, SearchView.OnQueryTextListener
{
    private lateinit var dataView: APIDataView

    private lateinit var messageListAdapter: MessagesListAdapter<MessageAdapter>

    private val messages = mutableListOf<MessageAdapter>()

    private val contacts = mutableMapOf<Int, ContactAdapter>()

    lateinit var conversation: Conversation

    private var addedAttachments = mutableListOf<UploadedAttachment>()

    private var capturedPicturePath: String? = null

    // This field needs to be stored as FileTarget() may be garbage collected, causing onBitmapLoaded callbacks never to come
    private var imageFileTarget: ImageDownloaderAdapter? = null

    private var replyToMessage: Message? = null

    private val errorContext = ErrorPool.createContext()

    private var mostRecentMessageFetched = false

    private var tasks = mutableListOf<TaskType>()

    private var rotationLocks = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conversation)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        config = loadConfiguration(this)
        dataView = DataViewFactory().createView(this, config)

        if (intent.hasExtra("notification_id")) // Cancel any notification that was clicked on
        {
            NotificationHandler.closeNotification(intent.getIntExtra("notification_id", 0))
        }

        if (intent.extras == null)
        {
            Log.e("ConversationActivity", "Called with null extra, stopping")
            finish()
            return
        }

        setupMessageList(intent.extras!!)

        input.setInputListener { sendMessage(it.toString()) }

        errorLabel.setOnClickListener { displayErrors() }
        input.setAttachmentsListener { onAddAttachment() }

        supportActionBar?.title = conversation.display_name
        setTaskDescription(ActivityManager.TaskDescription(conversation.display_name))

        if (savedInstanceState != null)
        {
            restoreState(savedInstanceState)
        }
    }

    private fun restoreState(bundle: Bundle)
    {
        if (bundle.containsKey("attachments"))
        {
            addedAttachments = bundle.getIntArray("attachments")!!.map { ObjectPool.get(it) as UploadedAttachment? }.filterNotNull().toMutableList()
            attachmentLabel.visibility = View.VISIBLE
            attachmentLabel.text = "${addedAttachments.count()} attachments uploaded"
        }

        if (bundle.containsKey("reply_to"))
        {
            val message = ObjectPool.get<Message>(bundle.getInt("reply_to"))

            if (message != null)
            {
                replyTo(message)
            }
            else
            {
                Log.e("Conversation", "Failed to load reply-to-message from object pool, discarding")
            }
        }
    }

    override fun onResume()
    {
        super.onResume()

        NotificationHandler.onConversationOpened(conversation)

        if (config.enableService)
        {
            val lastMessageInCache = dataView.cache.lookup<Message>(1).firstOrNull()
            val lastMessageInConversation = messages.maxBy { it.message.timestamp }?.message ?: lastMessageInCache

            startService(listOfNotNull(lastMessageInCache, lastMessageInConversation).maxBy { it.timestamp })
        }
        addTask(TaskType.REFRESH_UNSENT_MESSAGE)
    }

    override fun onStop()
    {
        super.onStop()

        NotificationHandler.onConversationClosed(conversation)
    }

    override fun onDestroy()
    {
        ErrorPool.freeContext(errorContext)
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_conversation, menu)

        val searchBar = menu.findItem(R.id.searchBar).actionView as SearchView
        searchBar.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)

        if (addedAttachments.isNotEmpty())
        {
            outState.putIntArray("attachments", addedAttachments.map { it.id }.toIntArray())
        }

        if (replyToMessage != null)
        {
            outState.putInt("reply_to", replyToMessage!!.id)
        }
    }

    private fun onAddAttachment()
    {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add attachment")
        builder.setPositiveButton("Take picture from camera") { _: DialogInterface, _: Int -> takePicture() }
        builder.setNegativeButton("Select file") { _: DialogInterface, _: Int -> uploadFile("*/*")}
        builder.setNeutralButton("Select Image") { _: DialogInterface, _: Int -> uploadFile("image/*")}

        builder.show()
    }

    private fun onCancelAttachments()
    {
        addedAttachments.clear()
        attachmentLabel.visibility = View.GONE
    }

    private fun uploadFile(type: String)
    {
        setRotationEnabled(false)
        val intent = Intent()
        intent.type = type
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_SELECT_FILE)
    }

    private fun setRotationEnabled(enabled: Boolean)
    {
        if (enabled)
        {
            rotationLocks--
            if (rotationLocks == 0)
            {
                setRotationEnabledImpl(true)
            }
        }
        else
        {
            rotationLocks++
            if (rotationLocks == 1)
            {
                setRotationEnabledImpl(false)
            }
        }
    }


    @SuppressLint("SourceLockedOrientationActivity")
    private fun setRotationEnabledImpl(enabled: Boolean)
    {
        requestedOrientation = if (enabled)
        {
            ActivityInfo.SCREEN_ORIENTATION_SENSOR
        }
        else
        {
            when (windowManager.defaultDisplay.rotation)
            {
                Surface.ROTATION_0 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                Surface.ROTATION_90 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                Surface.ROTATION_180 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                Surface.ROTATION_270 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                else -> throw RuntimeException("Unexpected screen rotation: ${resources.configuration.orientation}")
            }
        }
    }

    private fun takePicture()
    {
        if (CAMERA_PERMISSIONS.any { ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED})
        {
            requestPermissions(CAMERA_PERMISSIONS, REQUEST_CAMERA_PERMISSIONS)
            return
        }

        setRotationEnabled(false)
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())

        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val file = File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir)
        capturedPicturePath = file.absolutePath

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, "fr.bluecode.xlient", file))
        startActivityForResult(takePictureIntent, REQUEST_TAKE_IMAGE)
    }

    private fun uploadPicture()
    {
        val bitmap = BitmapFactory.decodeFile(capturedPicturePath)

        ByteArrayOutputStream().use { stream ->
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream) // TODO: Configurable quality

            uploadImpl(stream.toByteArray(), "picture.jpg")
        }

        capturedPicturePath = null
    }

    private fun extractFileNameFromUri(uri: Uri): String
    {
        if (uri.scheme != "content")
        {
            return uri.lastPathSegment!!
        }

        contentResolver.query(uri, null, null, null, null)!!.use {
            it.moveToFirst()
            return it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
        }
    }

    private fun uploadFile(intent: Intent)
    {
        val uri = intent.data as Uri
        val fileName = extractFileNameFromUri(uri).split("/").last()

        contentResolver.openInputStream(uri)!!.use { uploadImpl(it.readBytes(), fileName) }
    }

    private fun setDefaultTitle()
    {
        supportActionBar?.title = conversation.display_name
    }

    private fun onUploadProgress(done: Long, total: Long)
    {
       val title = "${Formatter.formatFileSize(this, done)} / ${Formatter.formatFileSize(this, total)}"
        Handler(mainLooper).post{ supportActionBar?.title = title }
    }

    private fun uploadImpl(content: ByteArray, name: String)
    {
        progressBar.visibility = View.VISIBLE
        dataView.uploadAttachment(
            {onAttachmentUploaded(it)},
            {onError(it, null)},
            name,
            ByteArrayInputStream(content),
            {done, total -> onUploadProgress(done, total)})
    }

    private fun onAttachmentUploaded(attachment: UploadedAttachment)
    {
        setDefaultTitle()
        setRotationEnabled(true)
        addedAttachments.add(attachment)
        attachmentLabel.visibility = View.VISIBLE
        attachmentLabel.text = "${addedAttachments.count()} attachments uploaded"

        progressBar.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_IMAGE && resultCode ==  Activity.RESULT_OK)
        {
            uploadPicture()
        }
        else if (requestCode == REQUEST_SELECT_FILE && resultCode == Activity.RESULT_OK)
        {
            uploadFile(data!!)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
    {
        if (requestCode == REQUEST_CAMERA_PERMISSIONS && grantResults.all{ it == PackageManager.PERMISSION_GRANTED })
        {
            takePicture()
        }
    }

    private fun showError(error: Exception)
    {
        ErrorPool.add(errorContext, error)

        errorLabel.text = "${ErrorPool.get(errorContext).count()} errors reported"
        errorLabel.visibility = View.VISIBLE

        progressBar.visibility = View.GONE
    }

    private fun displayErrors()
    {
        val intent = Intent(this, ErrorDisplayActivity::class.java)
        intent.putExtra("context", errorContext)

        startActivity(intent)
    }

    private fun getOrBuildAuthor(message: Message): ContactAdapter
    {
        if (!contacts.containsKey(message.author.id))
        {
            contacts[message.author.id] = ContactAdapter(message.author)
        }

        return contacts[message.author.id]!!
    }

    private fun getFirstUnreadMessage(messages: List<Message>): Message?
    {
        if (messages.isEmpty())
        {
            return null
        }

        val lastReadMessage = conversation.last_read_message
        if (lastReadMessage == null)
        {
            return null
        }

        return messages.firstOrNull{it.timestamp > lastReadMessage.timestamp}
    }

    private fun setupMessageList(bundle: Bundle)
    {
        val conversation_id = bundle.getInt("conversation")

        Log.i("ConversationActivity", "Setting up activity for conversation: $conversation_id")

        conversation = ObjectPool.get<Conversation>(conversation_id) ?: dataView.cache.lookupId(conversation_id) as Conversation

        for (e in conversation.errors)
        {
            showError(ErrorFoundOnBackendException(e, conversation.id))
        }

        val me = conversation.members.get().firstOrNull{it.is_me}

        val messagesToAdd = bundle.getIntArray("messages")!!.map { ObjectPool.get<Message>(it)!! }.sortedBy { it.timestamp }

        val firstUnreadMessage = getFirstUnreadMessage(messagesToAdd)
        for (e in messagesToAdd)
        {
            messages.addAll(createMessage(e, e.id == firstUnreadMessage?.id))
        }

        val holdersConfig = MessageHolders()
        holdersConfig.setOutcomingTextConfig(OutgoingTextMessageViewHolderAdapter::class.java, com.stfalcon.chatkit.R.layout.item_outcoming_text_message, this)
        holdersConfig.setIncomingTextConfig(IncomingTextMessageViewHolderAdapter::class.java, com.stfalcon.chatkit.R.layout.item_incoming_text_message, this)
        holdersConfig.setIncomingImageConfig(IncomingImageMessageViewHolderAdapter::class.java, com.stfalcon.chatkit.R.layout.item_incoming_image_message, this)
        holdersConfig.setOutcomingImageConfig(OutgoingImageMessageViewHolderAdapter::class.java, com.stfalcon.chatkit.R.layout.item_outcoming_image_message, this)

        messageListAdapter = MessagesListAdapter(me?.id?.toString() ?: "None", holdersConfig, ImageLoaderAdapter())
        messageList.setAdapter(messageListAdapter)

        messageList.setOnScrollChangeListener{_, _, _, _, _ -> onScroll()}
        messageListAdapter.addToEnd(messages.toList(), true) // TODO: Calling this method reverse our list, copy is needed
        messageListAdapter.setLoadMoreListener{_, total -> onHistoryScroll(total)}
        messageListAdapter.setOnMessageClickListener { onMessageClicked(it) }
        messageListAdapter.setOnMessageLongClickListener { onMessageLongClicked(it) }

        replyToLabel.setOnClickListener{onCancelReplyTo()}
        attachmentLabel.setOnClickListener{onCancelAttachments()}
    }

    private fun createMessage(message: Message, firstUnread: Boolean): List<MessageAdapter>
    {
        // Special case to avoid duplicating URL attachment and message texts
        if (message.attachments.count() == 1
            && message.attachments.first() is UrlAttachment
            && message.text.contains((message.attachments.first() as UrlAttachment).original_url ?: ""))
        {
            return listOf(MessageAdapter(message, getOrBuildAuthor(message), firstUnread))
        }


        val output = mutableListOf<MessageAdapter>()
        if (message.attachments.isEmpty() || message.text.isNotEmpty()) // Don't put empty messages for attachments
        {
            output.add(MessageAdapter(message, getOrBuildAuthor(message), firstUnread))
        }

        for (e in message.attachments)
        {
            when (e)
            {
                is ImageAttachment -> output.add(ImageMessageAdapter(message, e, getOrBuildAuthor(message), firstUnread))
                is UrlAttachment -> output.add(UrlMessageAdapter(message, e, getOrBuildAuthor(message), firstUnread))
                else -> output.add(UnsupportedAttachmentAdapter(message, e, getOrBuildAuthor(message), firstUnread))
            }
        }

        return output
    }

    private fun onMessageSent(message: Message)
    {
        addNewMessages(listOf(message), true)

        addedAttachments.clear()
        input.isEnabled = true
        attachmentLabel.visibility = View.GONE
        progressBar.visibility = View.GONE
        replyToLabel.visibility = View.GONE
        replyToMessage = null
    }

    private fun addNewMessages(messages: List<Message>, scroll: Boolean): Int
    {
        val sortedMessages = messages.sortedBy { it.timestamp }
        val firstUnread = getFirstUnreadMessage(this.messages.map { it.message } + messages)
        var added = 0

        for (e in sortedMessages.flatMap { createMessage(it, it.id == firstUnread?.id) })
        {
            val existingAdapter = this.messages.firstOrNull{it.id == e.id }
            if (existingAdapter != null)
            {
                messageListAdapter.update(existingAdapter.id, e)

                val index = this.messages.indexOf(existingAdapter)
                this.messages[index] = e
            }
            else
            {
                if (this.messages.isNotEmpty() && e.message.timestamp < this.messages.first().message.timestamp)
                {
                    continue // This message is older than our oldest message, probably comes from some old update through the WS. Ignore
                }
                added++
                this.messages.add(e)
                messageListAdapter.addToStart(e, scroll)
            }
        }

        if (messages.isNotEmpty())
        {
            val conversation = messages.maxBy { it.conversation!!.last_updated }!!.conversation!!

            if (conversation.last_updated > this.conversation.last_updated)
            {
                onConversationUpdated(conversation)
            }

            updateFirstUnreadMessage(firstUnread)
        }

        return added
    }

    private fun onError(error: Exception, text: String?)
    {
        showError(error)

        if (text != null)
        {
            input.inputEditText.setText(text + input.inputEditText.text)
        }
        setDefaultTitle()

        addedAttachments.clear()
        progressBar.visibility = View.GONE
        attachmentLabel.visibility = View.GONE
        input.isEnabled = true
    }

    private fun sendMessage(message: String): Boolean
    {
        dataView.sendMessage(
            {onMessageSent(it)},
            {onError(it, message)},
            conversation,
            message,
            if (addedAttachments.isEmpty()) null else addedAttachments,
            replyToMessage
        )

        input.isEnabled = false // Avoid race conditions. Only allow sending one message at a time
        progressBar.visibility = View.VISIBLE
        return true
    }

    private fun onMessagesLoaded(messages: List<Message>)
    {
        val sortedMessages = messages.sortedBy { it.timestamp }

        val mappedMessages = sortedMessages.map { createMessage(it, it.id == getFirstUnreadMessage(sortedMessages)?.id) }.flatten()

        // Don't scroll if the user has scrolled up
        messageListAdapter.addToEnd(mappedMessages.toList(), true)

        this.messages.addAll(0, mappedMessages)

        updateFirstUnreadMessage(getFirstUnreadMessage(this.messages.map { it.message }))

        setDefaultTitle()
        tasks.remove(TaskType.FETCH_NEW_MESSAGES)
    }

    private fun updateFirstUnreadMessage(firstUnread: Message?)
    {
        for (e in messages.filter { it.isFirstUnread() && it.message.id != firstUnread?.id})
        {
            e.firstUnread = false

            messageListAdapter.update(e)
        }
    }

    private fun updateFirstUnsentMessage(firstUnsent: SyntheticMessage)
    {
        if (firstUnsent.remote_id == null)
        {
            onTaskDone(TaskType.REFRESH_UNSENT_MESSAGE) // Message still not sent
        }

        for (e in messages.filter { it.message.id == firstUnsent.id })
        {
            e.message = firstUnsent
            messageListAdapter.update(e)
        }

        // Keep refreshing messages until we hit an unsent one or all have been refreshed
        refreshFirstUnsentMessage()
    }

    private fun refreshFirstUnsentMessage()
    {
        // The purpose of this method is to refresh a message that was sent by us.
        // In the case of a synthetic message, it's possible that the remote_id field was updated while the websocket
        // was disconnected, so to remove the orange square if needed we need to update the message's state manually
        val firstUnsent = messages.mapNotNull { it.message as SyntheticMessage }.sortedBy { it.timestamp }.firstOrNull { it.remote_id == null }
        if (firstUnsent == null)
        {
            onTaskDone(TaskType.REFRESH_UNSENT_MESSAGE) // Nothing to do, we're done
            return
        }

        Log.i("ConversationActivity", "Refreshing unsent message: ${firstUnsent.id}")
        dataView.fetchObject<SyntheticMessage>({updateFirstUnsentMessage(it)},
                                               {onError(it, null); onTaskDone(TaskType.REFRESH_UNSENT_MESSAGE)},
                                               firstUnsent.id)
    }

    private fun runNextTask()
    {
        if (tasks.isEmpty())
        {
            progressBar.visibility = View.GONE
            return
        }

        progressBar.visibility = View.VISIBLE

        val type = tasks.first()
        Log.i("Conversation", "Running task: $type, pending: ${tasks.map { it.name }}")
        if (type == TaskType.FETCH_NEW_MESSAGES)
        {
            dataView.getMessages(
                { onRecentMessagesFetched(it); onTaskDone(type) },
                { onError(it, null) },
                conversation,
                messages.last().message.id,
                "asc",
                config.messagesPerRequest
            )
        }
        else if (type == TaskType.FETCH_OLD_MESSAGES)
        {
            dataView.getMessages(
                { onMessagesLoaded(it); onTaskDone(type) },
                { onError(it, null) },
                conversation,
                messages.first().message.id,
                "desc",
                config.messagesPerRequest
            )
        }
        else if (type == TaskType.FETCH_CONVERSATION)
        {
            dataView.fetchObject<Conversation>(
                { onConversationFetched(it) }, // onConversationFetched schedules a callback that calls onTaskDone
                { onError(it, null) },
                conversation.id)
        }
        else if (type == TaskType.SCROLL_TO_FIRST_UNREAD)
        {
            scrollToFirstUnreadImpl()
        }
        else if (type == TaskType.REFRESH_UNSENT_MESSAGE)
        {
            refreshFirstUnsentMessage()
        }
    }

    private fun addTask(type: TaskType)
    {
        if (tasks.contains(type))
        {
            return
        }

        Log.i("Conversation", "Adding task: $type, pending: ${tasks.map { it.name }}")

        tasks.add(type)
        if (tasks.count() == 1)
        {
            runNextTask()
        }
    }

    private fun onTaskDone(type: TaskType)
    {
        tasks.remove(type)

        runNextTask()
    }

    private fun onHistoryScroll(total: Int)
    {
        if (total > messages.count())
        {
            addTask(TaskType.FETCH_OLD_MESSAGES)
        }
    }

    private fun scrollToFirstUnread()
    {
        addTask(TaskType.SCROLL_TO_FIRST_UNREAD)
    }

    private fun scrollToFirstUnreadImpl(fetched: Int = 0)
    {
        val index = messages.indexOfFirst { it.isFirstUnread() }
        if (index == -1 || !messages.any{it.message.id == conversation.last_read_message?.id})
        {
            if (conversation.last_read_message != null && conversation.last_read_message!!.timestamp > messages.last().message.timestamp)
            {
                Toast.makeText(this, "Last read message is past current scroll", Toast.LENGTH_LONG).show()
                onTaskDone(TaskType.SCROLL_TO_FIRST_UNREAD)
                return
            }
            if (messages.last().message.id == conversation.last_read_message?.id)
            {
                Toast.makeText(this, "All messages are read", Toast.LENGTH_LONG).show()
                onTaskDone(TaskType.SCROLL_TO_FIRST_UNREAD)
                return
            }
            else if (fetched > 200)
            {
                Toast.makeText(this, "First unread message not found after fetching $fetched messages", Toast.LENGTH_LONG).show()
            }
            else if (fetched != -1) // If fetch == -1, then we reached the top of the conversation. Stop fetching
            {
                // More messages need to be fetched
                dataView.getMessages(
                    { onMessagesLoaded(it); scrollToFirstUnreadImpl(if (it.isEmpty()) -1 else fetched + it.count()) },
                    { onError(it, null) },
                    conversation,
                    messages.firstOrNull()?.message?.id,
                    "desc",
                    config.messagesPerRequest
                )
                return
            }
        }

        messageList.smoothScrollToPosition(messages.count() - max(index, 0))
        onTaskDone(TaskType.SCROLL_TO_FIRST_UNREAD)
    }


    private fun onRecentMessagesFetched(messages: List<Message>)
    {
        if (messages.isEmpty())
        {
            mostRecentMessageFetched = true
        }
        else
        {
            addNewMessages(messages, false)
        }
    }

    private fun onScroll()
    {
        if (messageList.canScrollVertically(1) || mostRecentMessageFetched)
        {
            return // Bottom not reached yet, or we're already fetching
        }

        addTask(TaskType.FETCH_NEW_MESSAGES)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId)
        {
            R.id.action_refresh -> refresh()
            R.id.mark_as_read -> markAsRead()
            R.id.action_inspect -> inspect(conversation)
            R.id.action_go_to_first_unread -> scrollToFirstUnread()
            else -> super.onOptionsItemSelected(item)
        }

        return true
    }

    private fun inspect(value: DataModel)
    {
        assert(value is Message || value is Conversation)

        val intent = Intent(this, InspectObjectActivity::class.java)
        intent.putExtra("id", value.id)
        intent.putExtra("type", if (value is Message) "Message" else "Conversation")

        startActivity(intent)
    }

    private fun markAsRead()
    {
        if (messages.isEmpty())
        {
            Toast.makeText(this, "No messages in this conversation", Toast.LENGTH_LONG).show()
            return
        }

        dataView.markAsRead(
            {Toast.makeText(this, "Marked conversation as read", Toast.LENGTH_LONG).show(); onConversationUpdated(it.conversation!!)},
            {onError(it, null)},
            messages.maxBy { it.message.timestamp }!!.message,
            config.propagateReadUpdatesToBackends
        )
    }

    private fun refresh()
    {
        addTask(TaskType.FETCH_CONVERSATION)
        addTask(TaskType.REFRESH_UNSENT_MESSAGE)
    }

    private fun onConversationUpdated(conversation: Conversation)
    {
        Log.v("Conversation", "Conversation ${conversation.id} updated, refreshing view")

        this.conversation = conversation
        title = conversation.display_name

        for (e in messages) // Required as the message holders aren't updated when a message is
        {
            messageListAdapter.update(e)
        }

        updateFirstUnreadMessage(getFirstUnreadMessage(this.messages.map { it.message }))
    }

    private fun onConversationFetched(conversation: Conversation)
    {
        var status = ""
        if (conversation.last_updated > this.conversation.last_updated)
        {
            status = ", conversation updated"
            onConversationUpdated(conversation)
        }

        dataView.getMessages(
            {Toast.makeText(this, "Updated (${addNewMessages(it, true)} new messages$status)", Toast.LENGTH_LONG).show(); onTaskDone(TaskType.FETCH_CONVERSATION)},
            {onError(it, null)},
            conversation,
            from_id = messages.lastOrNull()?.message?.id,
            order = "asc"
        )
    }

    private fun onMessageClicked(message: MessageAdapter)
    {
        if (message !is ImageMessageAdapter || message.attachment !is FacebookImageAttachment || imageFileTarget != null)
        {
            return
        }

        val file = File("${config.cachePath}/images/${message.attachment.id}.jpg")
        File(file.parent).mkdirs()

        imageFileTarget = ImageDownloaderAdapter(
            {onImageDownloaded(it, file)},
            {onError(it, null); imageFileTarget = null},
            file
        )
        progressBar.visibility = View.VISIBLE
        Picasso.get().load(message.attachment.url).into(imageFileTarget!!)
    }

    private fun onMessageLongClicked(message: MessageAdapter)
    {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Actions for message ${message.message.id}")

        val actions = listOf(
            Pair("Reply") { replyTo(message.message) },
            Pair("Copy") { copyToClipboard(message.message) },
            Pair("React") { reactToMessage(message.message) },
            Pair("Inspect") { inspect(message.message) }
        )

        builder.setItems(actions.map { it.first }.toTypedArray()) { _, id -> actions[id].second()}
        builder.show()
    }

    private fun reactToMessage(message: Message)
    {
        if (message.conversation!!.source !is FacebookSource)
        {
            Toast.makeText(this, "Current source does not support reacts", Toast.LENGTH_LONG).show()
            return
        }

        val builder = AlertDialog.Builder(this)
        builder.setTitle("React to message ${message.id}")

        val reactions = listOf("❤", "😍", "😆", "😮", "😢", "😠", "👍", "👎", "Cancel")

        builder.setItems(reactions.toTypedArray()){_, id -> sendMessageReaction(message, reactions[id])}
        builder.show()
    }

    private fun sendMessageReaction(message: Message, reaction: String)
    {
        if (reaction == "Cancel")
        {
            return
        }

        progressBar.visibility = View.VISIBLE

        val content = mapOf("reaction" to mapOf("emoji" to reaction))

        dataView.reactToMessage(
            { updateMessage(it) },
            {onError(it, null)},
            message,
            content
        )
    }

    private fun updateMessage(message: Message)
    {
        for (e in messages.filter { it.message.id == message.id })
        {
            e.message = message
            messageListAdapter.update(e)
        }
        progressBar.visibility = View.GONE
    }

    private fun copyToClipboard(message: Message)
    {
        val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboardManager.primaryClip = ClipData.newPlainText("Message", message.text)

        Toast.makeText(this, "Copied to clipboard", Toast.LENGTH_SHORT).show()
    }

    private fun replyTo(message: Message)
    {
        replyToLabel.text = "Replying to: ${message.text.take(Math.min(50, message.text.count()))}"
        replyToLabel.visibility = View.VISIBLE

        replyToMessage = message
    }

    private fun onCancelReplyTo()
    {
        replyToLabel.visibility = View.GONE
        replyToMessage = null
    }

    private fun onImageDownloaded(bitmap: Bitmap, file: File)
    {
        FileOutputStream(file).use { stream ->
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream) // TODO: Configurable quality
        }

        // Hacky way around path exposure limitations
        StrictMode::class.java.getMethod("disableDeathOnFileUriExposure").invoke(null)

        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.parse("file://" + file.absolutePath), "image/*")
        startActivity(intent)

        progressBar.visibility = View.GONE
        imageFileTarget = null
    }

    override fun onObjectUpdated(dataModel: DataModel)
    {
        if (dataModel is Message && dataModel.conversation!!.id == conversation.id)
        {
            addNewMessages(listOf(dataModel), !messageList.canScrollVertically(1))
        }
        else if (dataModel is Conversation && dataModel.id == conversation.id && dataModel.last_updated > this.conversation.last_updated)
        {
            onConversationUpdated(dataModel)
        }
    }

    override fun onServiceError(error: Exception)
    {
        showError(error)
    }

    override fun onQueryTextSubmit(query: String?): Boolean
    {
        val intent = Intent(this, MessageSearchActivity::class.java)
        intent.putExtra("query", query!!)
        intent.putExtra("conversation", conversation.id)

        startActivity(intent)

        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean
    {
        return false
    }
}
