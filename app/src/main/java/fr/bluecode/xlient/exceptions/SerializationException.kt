package fr.bluecode.xlient.exceptions

class SerializationException(message: String): Exception(message)
{
}