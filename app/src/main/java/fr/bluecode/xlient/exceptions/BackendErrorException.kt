package fr.bluecode.xlient.exceptions

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.jsoniter.JsonIterator
import java.lang.Exception

fun makeMessage(response: Response): String
{
    return try
    {
        val json = JsonIterator.deserialize(response.data).asMap()

        "Backend error (HTTP ${response.statusCode}): code: ${json["error"].toString()}, details: ${json["details"]}"
    }
    catch (e: Exception)
    {
        "Error while reading backend error: $e, http code: ${response.statusCode}"
    }
}
class BackendErrorException(error: FuelError, response: Response) : HttpErrorException(error, makeMessage(response))
{
}