package fr.bluecode.xlient.exceptions

import fr.bluecode.xlient.api.model.Error
import java.io.Serializable
import java.lang.Exception

class ErrorFoundOnBackendException(val error: Error, val objectId: Int) : Exception(), Serializable