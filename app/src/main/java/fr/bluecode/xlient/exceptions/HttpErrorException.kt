package fr.bluecode.xlient.exceptions

import com.github.kittinunf.fuel.core.FuelError
import java.io.Serializable
import java.lang.Exception

open class HttpErrorException(fuelError: FuelError, message: String? = null) : Exception(message ?: "FuelError ${fuelError}"), Serializable
{
}