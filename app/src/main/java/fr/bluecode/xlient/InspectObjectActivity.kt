package fr.bluecode.xlient

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import fr.bluecode.xlient.api.Client
import fr.bluecode.xlient.api.Lazy
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.DatabaseHelper
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.loadConfiguration
import kotlinx.android.synthetic.main.activity_inspect_object.*
import kotlinx.android.synthetic.main.content_inspect_object.*
import java.util.*
import kotlin.reflect.full.memberProperties


@Suppress("UNCHECKED_CAST")
class InspectObjectActivity : AppCompatActivity(), AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
    data class Field(var name: String, var value: Any?)

    class Adapter(context: Context, fields: List<Field>, val name: String, val dataModel: DataModel?): ArrayAdapter<Field>(context, 0, fields)
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View
        {
            val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.object_field, parent,false)

            view.findViewById<TextView>(R.id.fieldName).text = getItem(position)?.name + " : "
            view.findViewById<TextView>(R.id.fieldValue).text = stringify(getItem(position)?.value)

            return view
        }

        private fun stringifyObject(value: DataModel): String
        {
            return "${value.javaClass.simpleName}(${value.id})"
        }

        private fun stringifyList(value: List<*>): String
        {
            return value.joinToString (prefix = "[", postfix = "]"){ if (it is DataModel) stringifyObject(it) else stringify(it) }
        }

        private fun stringify(value: Any?): String
        {
            return when(value)
            {
                null -> "null"
                is List<*> -> stringifyList(value)
                is Lazy<*> -> stringify(value.get())
                is DataModel -> stringifyObject(value)
                else -> value.toString()
            }
        }
    }

    private val objects = Stack<Adapter>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspect_object)
        setSupportActionBar(toolbar)

        val id = intent.getIntExtra("id", 0)

        val value = ObjectCache(
            loadConfiguration(
                this
            ), DatabaseHelper, ObjectPool).lookupId(id) ?: ObjectPool.get(id)!!
        push(buildAdapter(value))

        listView.onItemClickListener = this
        listView.onItemLongClickListener = this
    }

    override fun onBackPressed()
    {
        objects.pop()

        if (objects.empty())  // Leave activity if object stack is empty
        {
            super.onBackPressed()
        }
        else // Else go down one level
        {
            listView.adapter = objects.peek()
            title = objects.peek().name
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_inspect_object, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId)
        {
            R.id.action_view_local -> onInspectById(remote = false)
            R.id.action_view_remote -> onInspectById(remote = true)
            else -> super.onOptionsItemSelected(item)
        }

        return true
    }

    private fun onInspectById(remote: Boolean)
    {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Inspect object ID")

        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        builder.setView(input)

        builder.setPositiveButton("OK") { _, _ -> inspectByIdImpl(input.text.toString().toIntOrNull(), remote) }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    private fun inspectByIdImpl(id: Int?, remote: Boolean)
    {
        if (id == null)
        {
            return
        }

        var dataModel: DataModel? = null

        if (!remote)
        {
            dataModel = ObjectPool.get<DataModel>(id)
            if (dataModel == null)
            {
                Toast.makeText(this, "Object $id not found in object pool", Toast.LENGTH_LONG)
                    .show()
                return
            }
            push(buildAdapter(dataModel))
        }
        else
        {
            val client = Client(
                loadConfiguration(
                    this
                ), ObjectPool)

            client.fetchObject<DataModel>(
                {push(buildAdapter(it))},
                {Toast.makeText(this, "Error while fetching object $id: ${it.message}", Toast.LENGTH_LONG).show()},
                id
            )
        }
    }

    private fun buildAdapter(dataModel: List<DataModel>): Adapter
    {
        return Adapter(this, dataModel.mapIndexed{i: Int, e: DataModel -> Field(i.toString(), e)}, "Array", null)
    }

    private fun buildAdapter(value: DataModel): Adapter
    {
        return Adapter(this, value.javaClass.kotlin.memberProperties.map { Field(it.name, it.get(value)) }, "${value.javaClass.simpleName}(${value.id})", value)
    }

    override fun onItemLongClick(
        parent: AdapterView<*>?,
        view: View?,
        position: Int,
        id: Long
    ): Boolean
    {
        val value = objects.peek().getItem(position)!!.value

        val manager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        manager.primaryClip = ClipData.newPlainText("Xlient", value.toString())

        Toast.makeText(this, "Copied", Toast.LENGTH_LONG).show()

        return true
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
    {
        var value = objects.peek().getItem(position)!!.value
        if (value is Lazy<*>)
        {
            value = value.get()
        }

        if (value is DataModel)
        {
            push(buildAdapter(value))
            title = "${value.javaClass.simpleName}(${value.id})"
        }
        else if (value is List<*> && value.none{ it !is DataModel})
        {
            push(buildAdapter(value as List<DataModel>))
            title = "Array"
        }
        else if (value is Int && value != objects.peek().dataModel?.id)
        {
            inspectByIdImpl(value, remote = false)
        }
    }

    private fun push(adapter: Adapter)
    {
        objects.push(adapter)
        listView.adapter = objects.peek()

        title = adapter.name
    }
}
