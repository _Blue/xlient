package fr.bluecode.xlient

import android.annotation.SuppressLint
import kotlin.jvm.java
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import fr.bluecode.xlient.adapters.*
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.api.model.Facebook.FacebookConversation
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.background.IDataModelNotificationsHandler
import fr.bluecode.xlient.data.APIDataView
import fr.bluecode.xlient.exceptions.ErrorFoundOnBackendException
import fr.bluecode.xlient.settings.SettingsActivity
import fr.bluecode.xlient.data.DataViewFactory
import fr.bluecode.xlient.data.ErrorPool

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.errorLabel

val PERMISSIONS = listOf("android.permission.WRITE_EXTERNAL_STORAGE").toTypedArray()

const val REQUEST_PERMISSIONS_CODE = 1

class MainActivity : SyncedActivity(),
    SearchView.OnQueryTextListener, IDataModelNotificationsHandler
{
    private var searchBar: SearchView? = null

    private lateinit var dataView: APIDataView

    private lateinit var adapter: DialogsListAdapter<ConversationAdapter>

    private var conversationAdapters = mutableListOf<ConversationAdapter>()

    private var runningTasks = 0

    private var pendingTasks = mutableListOf<() -> Unit>()

    private var allConversationsVisible = false

    /*  This list is here to prevent the race condition where
        A conversation is in cache, but no message for that conversation is cached
        If the conversation is passed to onConversationsReady(), and the messages having been fetched
        from the backend yet, it is possible that another call will be scheduled because conversationAdapters
        wouldn't have a conversation object for that conversation yet
     */
    private var fetchedConversations = mutableSetOf<Int>()

    private var errorContext = ErrorPool.createContext()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        errorLabel.setOnClickListener{ viewErrors() }
        searchAllConversationsLabel.setOnClickListener{ addAllConversations() }
        searchInMessagesLabel.setOnClickListener{ searchInMessages() }

        adapter = DialogsListAdapter(com.stfalcon.chatkit.R.layout.item_dialog,  ConversationHolderAdapter::class.java, ImageLoaderAdapter())
        adapter.setDatesFormatter(TimeFormatterAdapter())
        dialogsList.setAdapter(adapter)

        adapter.setOnDialogClickListener { openDialog(it) }
    }

    override fun onResume()
    {
        super.onResume()

        if (config.endpointUrl == null || config.cachePath == null)
        {
            startActivityForResult(Intent(this, SettingsActivity::class.java), 0)
        }
        else if (PERMISSIONS.any{ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED})
        {
            requestPermissions(PERMISSIONS, REQUEST_PERMISSIONS_CODE)
        }
        else
        {
            initializeConversations()
        }
    }

    private fun initializeConversations()
    {
        dataView = DataViewFactory().createView(this, config)
        refresh()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
    {
        if (requestCode == REQUEST_PERMISSIONS_CODE && grantResults.all{ it == PackageManager.PERMISSION_GRANTED })
        {
            initializeConversations()
        }
    }

    override fun onDestroy()
    {
        ErrorPool.freeContext(errorContext)
        super.onDestroy()
    }

    private fun onTaskDone()
    {
        assert(runningTasks > 0)
        runningTasks--

        Log.v("MainActivity", "Task done, running tasks: ${runningTasks}, pending tasks: ${pendingTasks.count()}")

        if (runningTasks > 0)
        {
            return // Other tasks are still running, don't start pending tasks yet
        }

        val task = pendingTasks.firstOrNull()

        if (task == null)
        {
            updateLoadingStatus()
        }
        else
        {
            runningTasks++

            pendingTasks.removeAt(0)
            task()
        }
    }

    private fun updateLoadingStatus()
    {
        if (runningTasks == 0 && pendingTasks.isEmpty())
        {
            title = "Ready"
            progressBar.visibility = ProgressBar.GONE
        }
    }

    private fun refresh()
    {
        if (runningTasks > 0)
        {
            Toast.makeText(this, "Refresh is already in progress", Toast.LENGTH_SHORT).show()
            return
        }

        title = "Fetching conversations"
        progressBar.visibility = ProgressBar.VISIBLE

        pendingTasks.add{startServiceIfNeeded(); onTaskDone()} // Start the service once sync is done

        runningTasks++

        dataView.getConversations(
            {conversations, done -> onConversationsReady(conversations, done)},
            {error -> onError(error, true)}
        )
    }

    private fun openDialog(conversation: ConversationAdapter)
    {
        val intent = Intent(this, ConversationActivity::class.java)
        intent.putExtra("conversation", conversation.conversation.id)
        intent.putExtra("messages", conversation.getMessages().map { it.id }.toIntArray())

        startActivity(intent)
    }

    private fun viewErrors()
    {
        val intent = Intent(this, ErrorDisplayActivity::class.java)
        intent.putExtra("context", errorContext)

        startActivity(intent)
    }

    private fun onError(error: Exception, taskDone: Boolean)
    {
        if (taskDone)
        {
            onTaskDone()
        }
        ErrorPool.add(errorContext, error)

        errorLabel.text = "${ErrorPool.get(errorContext).count()} errors found"
        errorLabel.visibility = TextView.VISIBLE
        updateLoadingStatus()
    }

    private fun processConversation(conversation: Conversation)
    {
        for(error in conversation.errors.union(conversation.source.errors))
        {
            onError(ErrorFoundOnBackendException(error, conversation.id), false)
        }

        if (!fetchedConversations.contains(conversation.id)) // New conversation, add it
        {
            createConversation(conversation)
        }
        else // Conversation already exists, update it
        {
            val conversationAdapter = conversationAdapters.firstOrNull{it.conversation.id == conversation.id}
            if (conversationAdapter == null)
            {
                return // The conversation is being fetched, but hasn't been received yet
            }

            if (conversation.last_updated < conversationAdapter.conversation.last_updated)
            {
                return // Current conversation is more recent, drop the update
            }

            conversationAdapter.conversation = conversation

            if (!isFilteredBySearch(conversationAdapter))
            {
                adapter.upsertItem(conversationAdapter)
            }
        }
    }

    private fun onConversationsReady(conversations: List<Conversation>, done: Boolean)
    {
        val ts = System.currentTimeMillis()

        for(e in conversations.sortedBy { it.last_updated}.reversed())
        {
            processConversation(e)
        }

        Log.i("MainActivity", "Prepared ${conversations.count()} UI in ${System.currentTimeMillis() - ts} ms")

        if (done)
        {
            runningTasks--

            if (runningTasks == 0)  // If > 0, then some conversations messages are still being fetched, so don't fetch new messages yet
            {
                fetchNewMessages()
            }
            else
            {
                pendingTasks.add(0) {fetchNewMessages(); onTaskDone()} // Add first because this needs to be done before the service is started
            }
        }
    }

    private fun fetchNewMessages()
    {
        runningTasks++

        val lastMessage = conversationAdapters.mapNotNull { it.lastMessage?.message }.maxBy { it.timestamp }

        title = "Fetching new messages"

        dataView.getMessages(
            {onAllMessagesReady(it, 0)},
            {onError(it, true)},
            from_id = if (lastMessage?.id == EMPTY_MESSAGE_ID) null else lastMessage?.id,
            count = config.messagesPerRequest,
            order = "asc"
        )
    }

    private fun processMessages(messages: List<Message>)
    {
        for (e in messages.groupBy { it.conversation!! })
        {
            // find matching conversation
            val conversationAdapter = conversationAdapters.firstOrNull{it.conversation.id == e.key.id}
            if (conversationAdapter == null)
            {
                onMessagesReady(e.value, e.key)
            }
            else
            {
                conversationAdapter.conversation = e.key
                conversationAdapter.addMessages(e.value.map { MessageAdapter(it, ContactAdapter(it.author), false)})

                if (!isFilteredBySearch(conversationAdapter))
                {
                    adapter.upsertItem(conversationAdapter)
                }
            }
        }

        adapter.sortByLastMessageDate()
        updateLoadingStatus()
    }

    private fun onAllMessagesReady(messages: List<Message>, fetched: Int)
    {
        val done = messages.count() < config.messagesPerRequest

        if (!done) // If one query is not enough to get all the messages
        {
            title = "$fetched messages fetched"
            dataView.getMessages(
                {onAllMessagesReady(it, fetched + messages.count())},
                {onError(it, true)},
                from_id = messages.maxBy { it.timestamp }!!.id,
                order = "asc",
                count = config.messagesPerRequest
            )
        }

        processMessages(messages)

        if (done)
        {
            onTaskDone()
        }
    }

    private fun startServiceIfNeeded()
    {
        if (config.enableService)
        {
            startService(conversationAdapters.mapNotNull { it.lastMessage?.message }.maxBy { it.timestamp })
        }
    }

    private fun createConversation(conversation: Conversation)
    {
        onMessagesReady(if (conversation.last_message != null) listOf(conversation.last_message!!) else listOf(), conversation)
    }

    private fun onMessagesReady(messages: List<Message>, conversation: Conversation)
    {
        val existingAdapter = conversationAdapters.firstOrNull { it.conversation.id == conversation.id }
        val conversationAdapter = existingAdapter ?: ConversationAdapter(conversation)
        fetchedConversations.add(conversation.id)

        conversationAdapter.addMessages(messages.map { MessageAdapter(it, ContactAdapter(it.author), false)})

        val lastMessageTime = messages.maxBy { it.timestamp }?.timestamp ?: conversation.last_updated

        val insertAfter = conversationAdapters.indexOfLast { (if (it.lastMessage != null) (it.lastMessage as MessageAdapter).createdAt else it.conversation.last_updated) > lastMessageTime } + 1
        conversationAdapters.add(insertAfter, conversationAdapter)

        displayVisibleConversations(searchBar?.query?.toString())

        dialogsList.scrollToPosition(0)
        updateLoadingStatus()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_main, menu)
        searchBar = menu.findItem(R.id.searchBar).actionView as SearchView
        searchBar!!.setOnQueryTextListener(this)
        searchBar!!.queryHint = "Search"

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId)
        {
            R.id.action_settings -> startActivity(Intent(this, SettingsActivity::class.java))
            R.id.action_refresh -> refresh()
            R.id.action_search -> startActivity(Intent(this, MessageSearchActivity::class.java))
            R.id.action_show_all_conversations -> addAllConversations()
            else -> super.onOptionsItemSelected(item)
        }

        return true
    }

    private fun isFilteredBySearch(conversationAdapter: ConversationAdapter, queryText: String? = null): Boolean
    {
        if (searchBar == null) // UI not initialized yet
        {
            return false
        }

        val text = (queryText ?: searchBar!!.query.toString()).toLowerCase()

        if (text.isBlank())
        {
            return false
        }

        fun match(left: String): Boolean
        {
            return left.toLowerCase().contains(text)
        }

        return !(match(conversationAdapter.conversation.display_name)
                || conversationAdapter.conversation.members.get().any{match(it.display_name)}
                || conversationAdapter.conversation.gone_members.get().any{match(it.display_name)})
    }

    private fun displayVisibleConversations(searchQuery: String?)
    {
        if (searchQuery == null || searchQuery.isEmpty())
        {
            adapter.setItems(conversationAdapters)
        }
        else
        {
            adapter.setItems(conversationAdapters.filter { !isFilteredBySearch(it, searchQuery) })
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean
    {
        return false
    }

    override fun onQueryTextChange(query: String?): Boolean
    {
        displayVisibleConversations(query)

        searchInMessagesLabel.visibility = if (query.isNullOrEmpty()) View.GONE else View.VISIBLE

        if (query.isNullOrEmpty() || allConversationsVisible)
        {
            searchAllConversationsLabel.visibility = View.GONE
        }
        else
        {
            searchAllConversationsLabel.visibility = View.VISIBLE
        }
        return true
    }

    private fun addAllConversations()
    {
        val oldestConversation = conversationAdapters.minBy { it.conversation.last_updated }!!

        val conversations =  dataView.getOldConversations(oldestConversation.conversation.last_updated)

        for (e in conversations)
        {
            processConversation(e)
        }

        searchAllConversationsLabel.visibility = View.GONE
        allConversationsVisible = true
    }

    override fun onObjectUpdated(dataModel: DataModel)
    {
        if (dataModel is Message)
        {
            processMessages(listOf(dataModel))
        }
        else if (dataModel is Conversation)
        {
            processConversation(dataModel)
        }
    }

    override fun onServiceError(error: Exception)
    {
        onError(error, false)
    }

    private fun searchInMessages()
    {
        val intent = Intent(this, MessageSearchActivity::class.java)
        intent.putExtra("query", searchBar!!.query.toString())

        startActivity(intent)
    }
}
