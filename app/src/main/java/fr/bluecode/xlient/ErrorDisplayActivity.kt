package fr.bluecode.xlient

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import fr.bluecode.xlient.exceptions.ErrorFoundOnBackendException
import fr.bluecode.xlient.data.ErrorPool
import kotlinx.android.synthetic.main.content_error_display.*
import java.io.Serializable
import java.lang.Exception

class ErrorDisplayActivity : AppCompatActivity()
{
    lateinit var errors: List<Exception>

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error_display)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        if (savedInstanceState != null)
        {
            setupList(savedInstanceState)
        }
        else if(intent?.extras?.containsKey("context") == true)
        {
            setupList(intent.extras!!)
        }
    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)
        outState.putSerializable("errors", errors as Serializable)
    }

    override fun onRestoreInstanceState(bundle: Bundle?)
    {
        super.onRestoreInstanceState(bundle)
        setupList(bundle!!)
    }

    private fun formatError(error: Exception): String
    {
        return when(error)
        {
            is ErrorFoundOnBackendException -> "Backend error: "+ error.error.code
            else -> error.javaClass.canonicalName!!
        }
    }

    private fun setupList(bundle: Bundle)
    {
        errors = ErrorPool.get(bundle.getInt("context"))

        val adapter = ArrayAdapter<String>(this, R.layout.error_list_view, errors.map { formatError(it)})

        listview.adapter = adapter
        listview.setOnItemClickListener{ _, _, position, _ -> inspectError(position) }
    }

    private fun inspectError(position: Int)
    {
        val intent = Intent(this, SingleErrorDisplayActivity::class.java)
        intent.putExtra("error", errors[position])

        startActivity(intent)
    }
}
