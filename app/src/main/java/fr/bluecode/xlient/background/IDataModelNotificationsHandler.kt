package fr.bluecode.xlient.background

import fr.bluecode.xlient.api.model.DataModel
import java.lang.Exception

interface IDataModelNotificationsHandler
{
    fun onNewObject(dataModel: DataModel)

    fun onListenError(error: Exception)

    fun isInteractive(): Boolean
}