package fr.bluecode.xlient.background

import android.util.Log
import com.jsoniter.JsonIterator
import com.jsoniter.output.JsonStream
import fr.bluecode.xlient.api.Deserializer
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.settings.Configuration
import okhttp3.*
import java.lang.RuntimeException

class WebSocketClient (
    private val deserializer: Deserializer,
    private val config: Configuration,
    private val onReady: () -> Unit,
    private val onNewObject: (dataModel: DataModel) -> Unit,
    private val onError: (error: Exception) -> Unit ): WebSocketListener()
{
    private var ready = false

    private var webSocket: WebSocket? = null

    override fun onMessage(webSocket: WebSocket, text: String)
    {
        Log.i("Service", "Received websocket message: '$text'")

        val json = JsonIterator.deserialize(text)
        val content = json.asMap()

        if (!ready)
        {
            if (content["result"]?.toString() != "OK")
            {
                onError(RuntimeException("Error while initating websocket. Status is ${content["result"]?.toString()}. Message: $text"))
                webSocket.cancel()
            }
            else
            {
                ready = true
                onReady()
            }
            return
        }

        try
        {
            val dataModel = deserializer.deserializeJson(json) as DataModel
            onNewObject(dataModel)
        }
        catch (exception: Exception)
        {
            Log.e("Service", "Error while deserializing websocket response, $exception")
            onError(exception)
            return
        }

        super.onMessage(webSocket, text)
    }

    override fun onOpen(webSocket: WebSocket, response: Response)
    {
        super.onOpen(webSocket, response)

        Log.i("Service", "Websocket client connected")

        val message = mapOf<String, Any?>(
            "action" to "subscribe",
            "types" to listOf("Conversation", "Message"),
            "compress" to true)

        webSocket.send(JsonStream.serialize(message))
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?)
    {
        super.onFailure(webSocket, t, response)

        if (this.webSocket != null) // If false, then we have stopped listening, don't propagate the error
        {
            ready = false
            this.webSocket = null

            onError(t as Exception)
        }
    }

    fun listen()
    {
        assert(webSocket == null)

        Log.v("Service", "Websocket client starting")
        ready = false

        val request = Request.Builder().url((config.websocketEndpoint ?: config.endpointUrl) + "/ws/updates").build()
        webSocket = OkHttpClient.Builder().build().newWebSocket(request, this)
    }

    fun isListening(): Boolean
    {
        return webSocket != null
    }

    fun stop()
    {
        assert(webSocket != null)

        val socketRef = webSocket!!
        webSocket = null
        socketRef.cancel()

        ready = false
    }
}