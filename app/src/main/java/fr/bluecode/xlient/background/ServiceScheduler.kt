package fr.bluecode.xlient.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.content.ContextCompat.startForegroundService
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.DatabaseHelper
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.loadConfiguration

class ServiceScheduler : BroadcastReceiver()
{
    override fun onReceive(context: Context?, intent: Intent?)
    {
        val configuration = loadConfiguration(context!!)
        if (!configuration.startAtBoot)
        {
            Log.v("Service", "startAtBoot is false, not starting service")
            return
        }

        val objectCache = ObjectCache(configuration, DatabaseHelper, ObjectPool)
        val lastMessage = objectCache.getMostRecent<Message>("last_updated_child", mutableSetOf())

        if (lastMessage == null)
        {
            Log.v("Service", "LastMessage is null, not starting service")
            return
        }

        val startIntent = Intent(context, Service::class.java)
        startIntent.putExtra("last_message_id", lastMessage.id)
        startForegroundService(context, startIntent)
    }
}