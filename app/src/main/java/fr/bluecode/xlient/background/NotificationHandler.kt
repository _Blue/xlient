package fr.bluecode.xlient.background

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import fr.bluecode.xlient.ConversationActivity
import fr.bluecode.xlient.MainActivity
import fr.bluecode.xlient.R
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.ImageAttachment
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.api.model.UrlAttachment
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.settings.loadConfiguration
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object NotificationHandler
{
    class VisibleNotification(
        val id: Int,
        var messages: MutableList<Message>)

    private lateinit var configuration: Configuration
    private var initialized = false
    private val notifications = mutableListOf<VisibleNotification>()
    private lateinit var context: Context
    private lateinit var notificationManager: NotificationManager
    private val foregroundChannel = createNotificationChannel("service", "Service", false)
    private val errorChannel = createNotificationChannel("error", "Service errors", false)
    private val messageChannels = mutableMapOf<Int, NotificationChannel?>()
    private var openedConversation: Conversation? = null

    private var nextId = AtomicInteger(1)

    private var errorNotificationId = 10001;

    private var lock = ReentrantLock()

    fun initialize(context: Context)
    {
        lock.withLock {
            if (initialized)
            {
                return
            }

            this.context = context
            configuration = loadConfiguration(context)
            notificationManager =
                getSystemService(context, NotificationManager::class.java) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                notificationManager.createNotificationChannel(foregroundChannel!!)
                notificationManager.createNotificationChannel(errorChannel!!)
            }

            initialized = true
        }
    }

    private fun allocateId(): Int
    {
        return nextId.incrementAndGet();
    }

    private fun getOrCreateNotificationChannel(conversation: Conversation): NotificationChannel?
    {
        if (!messageChannels.containsKey(conversation.id))
        {
            val name = if (conversation.display_image.isNullOrBlank()) conversation.members.get().joinToString{ it.display_name } else conversation.display_name
            val channel = createNotificationChannel("conv.${conversation.id}", name, true)
            if (channel == null)
            {
                return null
            }

            messageChannels[conversation.id] = channel
            notificationManager.createNotificationChannel(channel)
        }

        return messageChannels[conversation.id]
    }

    fun onServiceRecovered()
    {
        lock.withLock { notificationManager.cancel(errorNotificationId) }
    }

    fun onServiceErrorNotificationClicked()
    {
        lock.withLock { notificationManager.cancel(errorNotificationId) }
    }

    fun onServiceError(text: String)
    {
        lock.withLock {
            val notificationIntent = Intent(context, MainActivity::class.java)
            notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK // Required to avoid intent mixups
            val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val broadcastIntent = Intent(RESET_SERVICE_ACTION)
            val restartServiceIntent = PendingIntent.getBroadcast(context, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            builder.setContentIntent(pendingIntent)
            builder.setContentTitle("Error in background service")
            builder.setContentText(text)
            builder.setCategory(Notification.CATEGORY_ERROR)
            builder.setStyle(NotificationCompat.BigTextStyle().bigText(text))
            builder.setSmallIcon(R.mipmap.ic_launcher)
            builder.addAction(R.drawable.bubble_circle, "Reset", restartServiceIntent)
            builder.setAutoCancel(true)

            if (errorChannel != null)
            {
                builder.setChannelId(errorChannel.id)
            }

            notificationManager.notify(errorNotificationId, builder.build())
        }
    }

    fun onConversationUpdated(conversation: Conversation)
    {
        if (conversation.last_read_message == null)
        {
            return
        }

        lock.withLock {
            val notification = notifications.firstOrNull { it.messages.first().conversation!!.id == conversation.id }
            if (notification == null)
            {
                return
            }

            if (!notification.messages.removeIf { it.timestamp <= conversation.last_read_message?.timestamp })
            {
                return
            }

            Log.v("Service", "Notification ${notification.id} updated due to read marker update")

            if (notification.messages.isEmpty())
            {
                closeNotification(notification.id)
            }
            else
            {
                showOrUpdateNotification(notification)
            }
        }

    }
    fun onConversationClosed(conversation: Conversation)
    {
        assert(openedConversation != null)

        openedConversation = null
    }

    fun onConversationOpened(conversation: Conversation)
    {
        assert(openedConversation == null)
        lock.withLock {
            openedConversation = conversation

            for (e in notifications.filter { it.messages.first().conversation!!.id == conversation.id }.toList())
            {
                closeNotification(e.id)
            }
        }
    }

    fun closeNotification(id: Int)
    {
        lock.withLock {
            notificationManager.cancel(id)

            if (!notifications.removeIf { it.id == id })
            {
                Log.e("Service", "Notification $id not found")
            }
        }
    }

    private fun shouldShowNotification(message: Message, notificationInConversation: VisibleNotification?): Boolean
    {
        if (notificationInConversation?.messages?.count() ?: 0 > configuration.maxNotificationPerConversation)
        {
            Log.v("Service", "Dropping notification for message ${message.id}, max per conversation reached")
            return false
        }
        else if (notifications.count() > configuration.maxNotifications)
        {
            Log.v("Service", "Dropping notification for message ${message.id}, max notification reached")
            return false
        }
        else if (notificationInConversation?.messages?.any{it.id == message.id} == true)
        {
            Log.v("Service", "Dropping notification for message ${message.id}, message already displayed")
            return false
        }
        else if (openedConversation?.id == message.conversation!!.id)
        {
            Log.v("Service", "Dropping notification for message ${message.id}, conversation is visible")
            return false
        }

        return true
    }

    private fun updateNotification(notification: VisibleNotification, newMessage: Message)
    {
        notification.messages.add(newMessage)
        notification.messages.sortBy { it.timestamp }
    }

    private fun createOrUpdateNotification(message: Message, existingNotification: VisibleNotification?): VisibleNotification
    {
        if (existingNotification != null)
        {
            updateNotification(existingNotification, message)
            return existingNotification
        }
        else
        {
            val notification = VisibleNotification(
                allocateId(),
                mutableListOf(message)
            )

            notifications.add(notification)

            return notification
        }
    }

    private fun getMessageText(message: Message): String
    {
        if (message.text.isBlank() && message.attachments.count() > 0)
        {
            if (message.attachments[0] is UrlAttachment)
            {
                val attachment = message.attachments[0] as UrlAttachment
                return attachment.original_url ?: attachment.url
            }
            else if (message.attachments[0] is ImageAttachment)
            {
                return "[Image]"
            }
            else
            {
                return message.attachments[0].javaClass.name
            }
        }
        else
        {
            return message.text
        }
    }

    private fun showOrUpdateNotification(notification: VisibleNotification)
    {
        val firstMessage = notification.messages.first()

        val notificationIntent = Intent(context, ConversationActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY // Required to avoid intent mixups
        notificationIntent.putExtra("conversation", firstMessage.conversation!!.id)
        notificationIntent.putExtra("messages", notification.messages.map { it.id }.toIntArray())
        val pendingIntent = PendingIntent.getActivity(context, firstMessage.conversation!!.id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val broadcastIntent = Intent(MARK_READ_ACTION)
        broadcastIntent.putExtra("message_id", firstMessage.id)
        broadcastIntent.putExtra("notification_id", notification.id)
        val markReadIntent = PendingIntent.getBroadcast(context, notification.id, broadcastIntent, PendingIntent.FLAG_IMMUTABLE)

        val text = notification.messages.joinToString(separator = "\r\n") { getMessageText(it) }
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
        builder.setContentIntent(pendingIntent)
        builder.setContentTitle(firstMessage.conversation!!.display_name)
        builder.setContentText(text)
        builder.setCategory(Notification.CATEGORY_MESSAGE)
        builder.setStyle(NotificationCompat.BigTextStyle().bigText(text))
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.addAction(R.drawable.bubble_circle, "Mark read", markReadIntent)
        builder.setAutoCancel(true)

        val channel = getOrCreateNotificationChannel(firstMessage.conversation!!)
        if (channel != null)
        {
            builder.setChannelId(channel.id)
        }

        notificationManager.notify(notification.id, builder.build())
    }

    fun onNewMessageReceived(message: Message)
    {
        lock.withLock {
            val notificationsInConversation = notifications.filter { it.messages.any { it.conversation!!.id == message.conversation!!.id } }
            assert(notificationsInConversation.count() <= 1)

            if (!shouldShowNotification(message, notificationsInConversation.firstOrNull()))
            {
                return
            }

            val notification =
                createOrUpdateNotification(message, notificationsInConversation.firstOrNull())
            showOrUpdateNotification(notification)
        }
    }

    fun buildForegroundNotification(): Notification
    {
        lock.withLock {
            val stopServiceIntent = Intent(context, Service::class.java)
            stopServiceIntent.action = STOP_SERVICE_ACTION
            stopServiceIntent.flags =
                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingStopServiceIntent = PendingIntent.getService(
                context, 0, stopServiceIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )

            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            builder.setContentText("Xocial sync is running")
            builder.setContentIntent(pendingStopServiceIntent)
            builder.setCategory(Notification.CATEGORY_SERVICE)
            builder.setVibrate(listOf<Long>(0).toLongArray())
            if (foregroundChannel != null)
            {
                builder.setChannelId(foregroundChannel.id)
            }

            return builder.build()
        }
    }

    private fun createNotificationChannel(tag: String, name: String, vibrate: Boolean): NotificationChannel?
    {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val channel = NotificationChannel(
                "fr.bluecode.xlient.$tag",
                name,
                if (vibrate) NotificationManager.IMPORTANCE_DEFAULT else NotificationManager.IMPORTANCE_LOW
            )
            channel.enableVibration(vibrate)

            if (!vibrate)
            {
                channel.setSound(null, null)
                channel.setShowBadge(false)
            }

            channel
        }
        else
        {
            null
        }
    }

    fun getMessagesForNotification(id: Int): List<Message>?
    {
        return lock.withLock { notifications.firstOrNull { it.id == id }?.messages }
    }

}