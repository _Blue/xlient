package fr.bluecode.xlient.background

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import fr.bluecode.xlient.adapters.EMPTY_MESSAGE_ID
import fr.bluecode.xlient.api.Deserializer
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.data.APIDataView
import fr.bluecode.xlient.data.DataViewFactory
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.settings.loadConfiguration
import java.util.*
import java.util.concurrent.RejectedExecutionException
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock


const val CHANNEL_ID = "fr.bluecode.xlient"

const val NOTIFICATION_ID = 1

const val STOP_SERVICE_ACTION = "fr.bluecode.xlient.stop_service"

const val MARK_READ_ACTION = "fr.bluecode.xlient.mark_read"

const val RESET_SERVICE_ACTION = "fr.bluecode.xlient.reset_service"


class Service : Service()
{
    enum class State
    {
        Starting,
        InitialSyncInteractive,
        InitialSyncBackground,
        RunningInteractive,
        RunningBackground,
        Backoff,
        NoConnection
    }

    private val binder = ServiceBinder(this)

    private var binderCount = 0

    private var configuration: Configuration? = null

    private lateinit var dataView: APIDataView

    private var lastMessage: Message? = null

    private var lastConversationUpdatedAt: Date? = null

    private var timer: Timer? = null

    private var running = false

    private lateinit var webSocket: WebSocketClient

    private lateinit var deserializer: Deserializer

    private lateinit var objectCache: ObjectCache

    private var expBackoffCounter: Long = 1

    private val lock = ReentrantLock()

    private lateinit var broadcastReceiver: Any

    var state: State = State.Starting

    private var connectionAvailable = true


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
    {
        if (intent?.action == STOP_SERVICE_ACTION)
        {
            Log.i("Service", "Service stop code received")
            stopForeground(0)
            stopSelf()
            return START_NOT_STICKY
        }

        lock.withLock {
            if (running)
            {
                return START_REDELIVER_INTENT
            }

            running = true

            return start(intent?.getIntExtra("last_message_id", 0) ?: 0)
        }
    }

    private fun transition(newState: State)
    {
        Log.i("Service", "Transition from state $state to $newState")

        state = newState
    }

    private fun initialize(last_message_id: Int)
    {
        Log.i("Service", "Initializing service in thread ${Thread.currentThread().id}")
        configuration = loadConfiguration(this)
        dataView = DataViewFactory().createView(this, configuration!!)

        deserializer = Deserializer(ObjectPool, true)
        objectCache = dataView.cache
        webSocket = WebSocketClient(deserializer,
                                    configuration!!,
                                    {onSuccessfulSync(true)},
                                    {onNewObject(it, mutableSetOf())},
                                    {onError(it)})
        var lastMessage = objectCache.lookupId(last_message_id) as Message?
        if (lastMessage == null)
        {
            Log.e("Service", "Failed to load message $lastMessage, fetching last message from cache")

            lastMessage = objectCache.getMostRecent<Message>("last_updated", mutableSetOf())
        }

        this.lastMessage = lastMessage
        lastConversationUpdatedAt = lastMessage?.last_updated

        NotificationHandler.initialize(this)

        broadcastReceiver = object :BroadcastReceiver()
        {
            override fun onReceive(context: Context?, intent: Intent?)
            {
                this@Service.onBroadcast(intent)
            }
        }

        registerReceiver(broadcastReceiver as BroadcastReceiver, IntentFilter(MARK_READ_ACTION))
        registerReceiver(broadcastReceiver as BroadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(broadcastReceiver as BroadcastReceiver, IntentFilter(RESET_SERVICE_ACTION));
    }

    private fun onBroadcast(intent: Intent?)
    {
        if (intent?.action == MARK_READ_ACTION)
        {
            val id = intent.getIntExtra("notification_id", 0)
            val messages = NotificationHandler.getMessagesForNotification(id)

            NotificationHandler.closeNotification(id)
            if (messages == null || messages.isEmpty())
            {
                Log.e("Service", "Failed to retrieve messages from notification $id")
                Toast.makeText(this, "Failed to retrieve messages from notification $id", Toast.LENGTH_LONG).show()
                return
            }

            val message = messages.maxBy { it.timestamp }!!
            dataView.markAsRead(
                {Toast.makeText(this, "Marked ${message.id}  as read", Toast.LENGTH_LONG).show()},
                {Toast.makeText(this, "Error while marking as read, $it", Toast.LENGTH_LONG).show()},
                message,
                configuration!!.propagateReadUpdatesToBackends
            )
        }
        else if (intent?.action == ConnectivityManager.CONNECTIVITY_ACTION)
        {
            onConnectivityChanged()
        }
        else if (intent?.action == RESET_SERVICE_ACTION)
        {
            lock.withLock { expBackoffCounter = 1;  performInitialSync() }
            NotificationHandler.onServiceErrorNotificationClicked()
        }
    }

    private fun onConnectivityChanged()
    {
        val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val connected = manager.activeNetworkInfo?.isConnected == true

        if (connectionAvailable != connected)
        {
            connectionAvailable = connected
            lock.withLock { onConnectivityChangedImpl(connected) }
        }
        else if (state == State.Backoff) // In case the backend is accessible only from one connection (ex: VPN)
        {
            expBackoffCounter = 1
            performInitialSync()
        }
    }

    private fun onConnectivityChangedImpl(connected: Boolean)
    {
        if (connected && (state == State.NoConnection || state == State.Backoff))
        {
            Log.i("Service", "Connectivity recovered, starting notification poll")

            performInitialSync()
        }
        else if (!connected && state != State.NoConnection)
        {
            Log.i("Service", "Connectivity lost, stopping notification poll")

            transition(State.NoConnection)
            resetTimer()

            if (webSocket.isListening())
            {
                webSocket.stop()
            }
        }
    }

    private fun start(last_message_id: Int): Int
    {
        Log.i("Service", "Notification service starting")

        initialize(last_message_id)

        startForeground(NOTIFICATION_ID, NotificationHandler.buildForegroundNotification())

        performInitialSync()
        return START_STICKY
    }

    private fun onSuccessfulSync(fromWebSocket: Boolean)
    {
        NotificationHandler.onServiceRecovered()

        if (fromWebSocket) // This block is here to avoid dropping messages while the websocket is connecting
        {
            dataView.getMessages(
                { onNewMessages(it); if (state == State.InitialSyncInteractive) transition(State.RunningInteractive) },
                { onError(it) },
                from_id = if (lastMessage?.id == EMPTY_MESSAGE_ID) null else lastMessage?.id,
                order = "asc"
            )
        }
        else if (state == State.InitialSyncBackground)
        {
            transition(State.RunningBackground)
        }

        expBackoffCounter = 1
    }

    override fun onBind(intent: Intent?): IBinder?
    {
        binderCount++

        if (configuration !=  null) // in case the service was running in background mode, bring interactive mode back on
        {
            onInteractiveStateChange()
        }
        return binder
    }

    override fun onRebind(intent: Intent?)
    {
        onBind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean
    {
        binderCount--

        if (binderCount == 0) // Everyone disconnected, enter daemon mode
        {
            onInteractiveStateChange()
        }

        return true
    }

    private fun isInteractive(): Boolean
    {
        return lock.withLock { binder.interactive }
    }

    fun onInteractiveStateChange()
    {
        lock.withLock {
            if (state == State.NoConnection || state == State.Backoff || state == State.Starting)
            {
                return;
            }

            if (isInteractive() && state != State.RunningInteractive && state != State.InitialSyncInteractive)
            {
                Log.i("Service", "Stopping polling and starting interactive mode")
                performInitialSync()
            }
            else if (!isInteractive() && state != State.RunningBackground && state != State.InitialSyncBackground)
            {
                Log.i("Service", "Stopping interactive mode and starting polling")
                expBackoffCounter = 1 // Reset backoff counter
                startPolling()
            }
        }
    }

    private fun resetTimer()
    {
        timer?.cancel()
        timer = Timer()
    }

    private fun startPolling()
    {
        Log.i("Service", "Scheduling poll. Interactive: ${isInteractive()}")

        transition(State.InitialSyncBackground)

        resetTimer()
        if (webSocket.isListening())
        {
            webSocket.stop()
        }

        if (isInteractive())
        {
            scheduleTimer(configuration!!.pollingWait.toLong() * 1000)
        }
        else
        {
            scheduleTimer(configuration!!.backgroundPollingRefresh.toLong() * 1000)
        }
    }

    private fun tick()
    {
        if (!connectionAvailable) // Can happen if service is manually reset while connection is down
        {
            resetTimer()
            transition(State.NoConnection)
        }
        else if (state == State.Backoff)
        {
            Log.i("Service", "Backoff tick received, restarting")
            performInitialSync()
        }
        else if (state == State.RunningBackground || state == State.InitialSyncBackground)
        {
            if (state == State.InitialSyncBackground)
            {
                transition(State.RunningBackground)
            }

            Log.i("Service", "Polling")
            fetchNewMessages()
        }
    }

    private fun scheduleTimer(periodMs: Long)
    {
        val task = object: TimerTask()
        {
            override fun run()
            {
                lock.withLock { tick() };
            }
        }

        Log.v("Service", "Scheduling timer with period: $periodMs ms, backoff: $expBackoffCounter")

        timer!!.scheduleAtFixedRate(task, expBackoffCounter * 1000, periodMs)
    }

    private fun showNewMessageNotification(message: Message)
    {
        objectCache.cache(message) // The notification might stay after the death of your main process, on-disk cache is needed

        NotificationHandler.onNewMessageReceived(message)
    }

    private fun notify(dataModel: DataModel)
    {
        for (e in binder.consumers)
        {
            e.onNewObject(dataModel)
        }
    }

    private fun onNewObject(dataModel: DataModel, cached: MutableSet<Int>)
    {
        lock.withLock { onNewObjectImpl(dataModel, cached) }
    }

    private fun onNewObjectImpl(dataModel: DataModel, cached: MutableSet<Int>)
    {
        if (dataModel is Message)
        {
            if (lastMessage == null || dataModel.timestamp > lastMessage!!.timestamp)
            {
                lastMessage = dataModel
            }

            processNewMessage(dataModel)
            objectCache.cache(dataModel, cached)
        }
        else if (dataModel is Conversation)
        {
            if (dataModel.last_read_message != null)
            {
                NotificationHandler.onConversationUpdated(dataModel)
            }

            Log.v("Service", "Caching conversation ${dataModel.id}")

            objectCache.cache(dataModel, cached)
        }

        notify(dataModel)
    }

    private fun processNewMessage(message: Message)
    {
        val read = message.conversation!!.last_read_message != null
                            && message.conversation!!.last_read_message!!.timestamp >= message.timestamp

        if (message.author.is_me || read)  // Don't display a notification for user's own messages
        {
            NotificationHandler.onConversationUpdated(message.conversation!!) // If the user sent a new message, then the conversation is read
            return
        }

        showNewMessageNotification(message)
    }

    private fun onNewMessages(messages: List<Message>)
    {
        Log.v("Service", "Received messages: ${messages.map { it.id }}")

        val cachedObjects = mutableSetOf<Int>()
        for (e in messages)
        {
            onNewObjectImpl(e, cachedObjects)
        }
    }

    private fun refreshConversations()
    {
        try
        {
            dataView.getConversations(
                lastConversationUpdatedAt ?: Date(0),
                "asc",
                {lock.withLock { onNewConversations(it); onSuccessfulSync(false)}},
                {onError(it)});
        }
        catch (e: RejectedExecutionException) // Can happen when emulators are woken up from sleep and all events fire at once
        {
            onError(e)
        }
    }

    private fun fetchNewMessages()
    {
        try
        {
            // Note: If the last message id is -1, then the database is completely empty
            dataView.getMessages(
                { lock.withLock { onNewMessages(it); refreshConversations()}},
                {onError(it)},
                from_id = if (lastMessage?.id == EMPTY_MESSAGE_ID) null else lastMessage?.id,
                order = "asc"
            )
        }
        catch (e: RejectedExecutionException) // Can happen when emulators are woken up from sleep and all events fire at once
        {
            onError(e)
        }
    }

    private fun onNewConversations(conversations: List<Conversation>)
    {
        if (conversations.isEmpty())
        {
            return
        }

        val cached = mutableSetOf<Int>()

        lock.withLock {
            for (e in conversations)
            {
                onNewObject(e, cached)
            }

            lastConversationUpdatedAt = conversations.maxBy { it.last_updated }!!.last_updated
        }
    }

    private fun onError(error: Exception)
    {
        lock.withLock {

            Log.e("Service", "Error while fetching new messages, backoff = $expBackoffCounter", error)

            for (e in binder.consumers)
            {
                e.onListenError(error)
            }

            NotificationHandler.onServiceError("Service is failed due to ${error.javaClass}, backoff = $expBackoffCounter")
            if (!connectionAvailable || state == State.NoConnection)
            {
                transition(State.NoConnection) // Don't start retrying when network connectivity is down
                return;
            }


            expBackoffCounter *= 2

            transition(State.Backoff)
            lock.withLock { resetTimer(); scheduleTimer(1000) }
        }
    }

    private fun performInitialSync()
    {
        Log.v("Service", "Performing initial sync, last message = ${lastMessage?.id}")

        resetTimer()

        if (isInteractive() && configuration!!.enableWebsocket)
        {
            transition(State.InitialSyncInteractive)

            if (!webSocket.isListening())
            {
                webSocket.listen()
            }
        }
        else
        {
            startPolling() // Will make transition to InitialSyncBackground
        }
    }
}