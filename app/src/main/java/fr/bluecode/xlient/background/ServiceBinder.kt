package fr.bluecode.xlient.background

import android.os.Binder
import android.util.Log

class ServiceBinder(private val service: Service) : Binder()
{
    val consumers = mutableListOf<IDataModelNotificationsHandler>()

    var interactive = false

    fun subscribe(consumer: IDataModelNotificationsHandler)
    {
        Log.v("Service", "Subscribed to binder: ${consumer.javaClass.canonicalName}")
        consumers.add(consumer)

        onInteractiveStateChange()
    }

    fun unsubscribe(consumer: IDataModelNotificationsHandler)
    {
        Log.v("Service", "Unsubscribed from binder: ${consumer.javaClass.canonicalName}")
        consumers.remove(consumer)
        onInteractiveStateChange()
    }

    fun onInteractiveStateChange()
    {
        val newState = consumers.any{ it.isInteractive() }

        if (newState != interactive)
        {
            interactive = newState
            service.onInteractiveStateChange()
            Log.v("Service", "New interactive state $newState")
        }
    }

    fun getServiceState(): Service.State
    {
        return service.state
    }
}