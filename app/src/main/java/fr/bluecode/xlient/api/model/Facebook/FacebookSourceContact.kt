package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.Source
import fr.bluecode.xlient.api.model.SourceContact
import java.util.*

open class FacebookSourceContact(
    id: Int,
    last_updated: Date,
    display_name: String,
    display_image: String?,
    source: Source,
    is_me: Boolean,
    val facebook_user_id: String
): SourceContact(id, last_updated, display_name, display_image, source, is_me)