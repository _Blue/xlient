package fr.bluecode.xlient.api.model

import java.util.*

open class MessageReaction(
    id: Int,
    last_updated: Date,
    val author: Int
    ) : DataModel(id, last_updated)
