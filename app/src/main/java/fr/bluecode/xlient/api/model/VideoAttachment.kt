package fr.bluecode.xlient.api.model

import java.util.*

open class VideoAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    var url: String,
    var length_ms: Int?) : Attachment(id, last_updated, hosted_url)
