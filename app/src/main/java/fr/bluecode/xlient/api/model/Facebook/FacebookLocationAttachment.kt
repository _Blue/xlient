package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.LocationAttachment
import java.util.*

open class FacebookLocationAttachment(
    id: Int,
    hosted_url: String?,
    var facebook_attachment_id: String,
    last_updated: Date,
    latitude: Double?,
    longitude: Double?,
    image_url: String?,
    map_url: String?
) : LocationAttachment(id, last_updated, hosted_url, latitude, longitude, image_url, map_url)