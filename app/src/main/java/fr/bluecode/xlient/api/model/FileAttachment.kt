package fr.bluecode.xlient.api.model

import java.util.*

open class FileAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    var url: String,
    var name: String?,
    var size: Int?) : Attachment(id, last_updated, hosted_url)
