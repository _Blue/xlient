package fr.bluecode.xlient.api.model

import java.util.*

open class Source(
    id: Int,
    last_updated: Date,
    val display_name: String,
    val status: String,
    val refresh_time: Double,
    val errors: List<Error>
) : DataModel(id, last_updated)
