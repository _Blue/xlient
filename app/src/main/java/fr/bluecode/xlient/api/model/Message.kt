package fr.bluecode.xlient.api.model

import java.util.*

open class Message(
    id: Int,
    last_updated: Date,
    val text: String,
    val author: Contact,
    var conversation: Conversation?, // Needs to be nullable because of circular references during instanciation
    val edited_timestamp: Date?,
    val timestamp: Date,
    val attachments: List<Attachment>,
    val reply_to: Message?,
    val reactions: List<MessageReaction>
    ): DataModel(id, last_updated)