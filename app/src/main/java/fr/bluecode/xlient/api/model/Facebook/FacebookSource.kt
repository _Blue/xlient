package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.Source
import fr.bluecode.xlient.api.model.Error
import java.util.*

class FacebookSource(
    id: Int,
    last_updated: Date,
    display_name: String,
    status: String,
    refresh_time: Double,
    errors: List<Error>,
    val user_email: String,
    val facebook_user_id: String
    ) : Source(id, last_updated, display_name, status, refresh_time, errors)