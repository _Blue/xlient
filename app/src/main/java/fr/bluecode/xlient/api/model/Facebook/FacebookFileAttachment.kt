package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.FileAttachment
import java.util.*

open class FacebookFileAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    var facebook_attachment_id: String,
    url: String,
    name: String?,
    size: Int?
) : FileAttachment(id, last_updated, hosted_url, url, name, size)