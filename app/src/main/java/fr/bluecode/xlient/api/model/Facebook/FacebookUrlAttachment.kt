package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.UrlAttachment
import java.util.*


open class FacebookUrlAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val facebook_attachment_id: String,
    url: String,
    original_url: String?,
    title: String?,
    description: String?
) : UrlAttachment(id, last_updated, hosted_url, url, title, description, original_url)