package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.MessageReaction
import java.util.*

class FacebookMessageReaction(
    id: Int,
    last_updated: Date,
    author: Int,
    val emoji: String
) : MessageReaction(id, last_updated, author)
