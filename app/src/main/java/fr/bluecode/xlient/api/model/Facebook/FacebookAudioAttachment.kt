package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.AudioAttachment
import java.util.*

open class FacebookAudioAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val facebook_attachment_id: String,
    url: String,
    name: String?,
    length_ms: Int?
) : AudioAttachment(id, last_updated, hosted_url, url, name, length_ms)