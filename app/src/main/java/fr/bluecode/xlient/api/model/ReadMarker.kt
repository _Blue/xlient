package fr.bluecode.xlient.api.model

import java.util.*

class ReadMarker(
    id: Int,
    last_updated: Date,
    val contact: Int,
    val timestamp: Date,
    val message: Int)
    : DataModel(id, last_updated)
