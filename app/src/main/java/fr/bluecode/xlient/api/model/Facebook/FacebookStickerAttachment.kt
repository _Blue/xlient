package fr.bluecode.xlient.api.model.Facebook

import java.util.*

open class FacebookStickerAttachment(
    id: Int,
    last_updated: Date,
    facebook_attachment_id: String,
    hosted_url: String?,
    val facebook_sticker_pack_id: String?,
    val facebook_label: String?,
    url: String,
    preview_url: String?
) : FacebookImageAttachment(id, last_updated, hosted_url, facebook_attachment_id, url, preview_url)