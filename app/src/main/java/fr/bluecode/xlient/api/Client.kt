package fr.bluecode.xlient.api

import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.jsoniter.JsonIterator
import com.jsoniter.output.JsonStream
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.exceptions.BackendErrorException
import fr.bluecode.xlient.exceptions.HttpErrorException
import fr.bluecode.xlient.settings.Configuration
import java.io.InputStream
import java.net.URLEncoder
import java.util.concurrent.*
import kotlin.reflect.KClass

class Client(val configuration: Configuration, objectPool: ObjectPool)
{
    val deserializer: Deserializer = Deserializer(objectPool, true)
    private val manager = FuelManager()

    init
    {
        val threadFactory = ThreadFactory()
        {
            Thread(it).also { thread ->
                thread.priority = Thread.NORM_PRIORITY
                thread.isDaemon = true
            }
        }

        manager.executorService = ThreadPoolExecutor(
            0, configuration.maxParallelRequests,
            60L, TimeUnit.SECONDS,
            ArrayBlockingQueue<Runnable>(10000),
            threadFactory
        )

    }

    fun createError(fuelError: FuelError, response: Response): Exception
    {
        return if (response.statusCode >= 200)
        {
            BackendErrorException(fuelError, response)
        }
        else
        {
            HttpErrorException(fuelError)
        }
    }

    private fun configureRequest(request: Request): Request
    {
        return request.timeout(configuration.timeout)
            .timeoutRead(configuration.timeout)
            .appendHeader("X-Xocial-Use-Compact-Serialization", if (configuration.enableCompactSerialization) "1" else "0" )
    }

    fun getRequest(url: String): Request
    {
        return configureRequest(manager.get("${configuration.endpointUrl}/$url"))
    }

    fun postRequest(url: String): Request
    {
        return configureRequest(manager.post("${configuration.endpointUrl}/$url"))
    }

    inline fun <reified T> fetchObject(
        crossinline success: (result: T) -> Unit,
        crossinline failure: (error: Exception) -> Unit,
        id: Int
    )
    {
        getRequest("object/$id").response { _, response, result ->
            when (result)
            {
                is Result.Failure -> failure(createError(result.error, response))
                is Result.Success -> deserializeResponse(result.value.inputStream(), success, failure)
            }
        }
    }

    inline fun <reified T> fetchObjects(
        crossinline success: (result: List<T>) -> Unit,
        crossinline failure: (error: Exception) -> Unit,
        route: String = T::class.simpleName!!.toLowerCase(),
        arguments: Map<String, String> = mapOf()
    )
    {
        var url = route

        if (arguments.isNotEmpty())
        {
            url += arguments.map { "${it.key}=${URLEncoder.encode(it.value, "utf-8")}"}.joinToString("&", prefix = "?")
        }

        getRequest(url).response { _, response, result ->
            when (result)
            {
                is Result.Failure -> failure(createError(result.error, response))
                is Result.Success -> deserializeResponse<List<T>>(result.value.inputStream(), success, failure)
            }
      }
    }

    inline fun <reified T> deserializeResponse(
        response: InputStream,
        crossinline success: (result: T) -> Unit,
        crossinline failure: (error: Exception) -> Unit
    )
    {
        val output = try
        {
            deserializer.deserialize(response.readBytes()) as T
        }
        catch (e: Exception)
        {
            failure(e)
            return
        }

        success(output)
    }

    inline fun <reified T> post(
        crossinline success: (result: T) -> Unit,
        crossinline failure: (error: Exception) -> Unit,
        route: String,
        body: ByteArray?,
        useDeserializer: Boolean = true,
        noinline progress: ProgressCallback? = null
    )
    {
        var request = postRequest(route)
        if (body != null)
        {
            request = request.body(body)
        }

        if (progress != null)
        {
            request = request.requestProgress(progress)
        }

        request.response{ _, response, result ->
            when (result)
            {
                is Result.Failure -> failure(createError(result.error, response))
                is Result.Success -> {
                    if (useDeserializer)
                    {
                        deserializeResponse<T>(result.value.inputStream(), success, failure)
                    }
                    else
                    {
                        success(JsonIterator.deserialize(result.value, T::class.java))
                    }
                }
            }
        }
    }

    inline fun <reified T> post(
        crossinline success: (result: T) -> Unit,
        crossinline failure: (error: Exception) -> Unit,
        route: String,
        body: Map<String, Any?>? = null,
        useDeserializer: Boolean = true
    )
    {
        val json = if (body != null) JsonStream.serialize(body).toByteArray() else null
        return post<T>(success, failure, route, json, useDeserializer)
    }
}