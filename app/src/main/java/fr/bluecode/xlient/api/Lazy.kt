package fr.bluecode.xlient.api


class Lazy<T>(private val lookup: (()-> T)) where T: Any
{
    private var ready = false

    private lateinit var value: T

    fun get(): T
    {
        if (!ready)
        {
            value = lookup()
            ready = true
        }

        return value
    }
}