package fr.bluecode.xlient.api.model

import fr.bluecode.xlient.api.Lazy
import java.util.*

open class Conversation(
    id: Int,
    last_updated: Date,
    val display_name: String,
    val display_image: String?,
    val members: Lazy<List<Contact>>,
    val gone_members: Lazy<List<Contact>>,
    val source: Source,
    val errors: List<Error>,
    var last_read_message: Message?,
    var last_message: Message?,
    val read_markers: List<ReadMarker>,
    val unread_count: Int
) : DataModel(id, last_updated)