package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.ImageAttachment
import java.util.*

open class FacebookImageAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    var facebook_attachment_id: String,
    url: String,
    preview_url: String?
) : ImageAttachment(id, last_updated, hosted_url, url, preview_url)