package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.Lazy
import fr.bluecode.xlient.api.model.*
import java.util.*

open class FacebookConversation(
    id: Int,
    last_updated: Date,
    display_name: String,
    display_image: String?,
    members: Lazy<List<Contact>>,
    gone_members: Lazy<List<Contact>>,
    source: Source,
    errors: List<Error>,
    last_read_message: Message?,
    last_message: Message?,
    read_markers: List<ReadMarker>,
    unread_count: Int,
    val facebook_conversation_id: String,
    val facebook_conversation_type: String,
    val facebook_last_message_timestamp: Date?
): Conversation(id, last_updated, display_name, display_image, members, gone_members, source, errors, last_read_message, last_message, read_markers, unread_count)