package fr.bluecode.xlient.api.model

import java.util.*

open class UploadedAttachment(
    id: Int,
    last_updated: Date,
    val size: Int?,
    val file_name: String
) : DataModel(id, last_updated)