package fr.bluecode.xlient.api.model.Synthetic

import fr.bluecode.xlient.api.model.Error
import fr.bluecode.xlient.api.model.Source
import java.util.*

class SyntheticSource(
    id: Int,
    last_updated: Date,
    display_name: String,
    status: String,
    refresh_time: Double,
    errors: List<Error>
) : Source(id, last_updated, display_name, status, refresh_time, errors)
