package fr.bluecode.xlient.api.model

import java.util.*

open class AudioAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val url: String,
    val name: String?,
    val length_ms: Int?) : Attachment(id, last_updated, hosted_url)
