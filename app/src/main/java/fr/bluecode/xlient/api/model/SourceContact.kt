package fr.bluecode.xlient.api.model

import java.util.*

open class SourceContact(
    id: Int,
    last_updated: Date,
    val display_name: String,
    val display_image: String?,
    val source: Source,
    val is_me: Boolean
): DataModel(id, last_updated)