package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.*
import java.util.*

open class FacebookMessage(
    id: Int,
    last_updated: Date,
    text: String,
    author: Contact,
    conversation: Conversation?,
    edited_timestamp: Date?,
    timestamp: Date,
    attachments: List<Attachment>,
    reply_to: Message?,
    reactions: List<MessageReaction>,
    val facebook_message_id: String
): Message(id, last_updated, text, author, conversation, edited_timestamp, timestamp, attachments, reply_to, reactions)