package fr.bluecode.xlient.api.model

import java.io.Serializable
import java.util.*

open class Error(
    id: Int,
    last_updated: Date,
    val code: String,
    val details: String
) : DataModel(id, last_updated), Serializable