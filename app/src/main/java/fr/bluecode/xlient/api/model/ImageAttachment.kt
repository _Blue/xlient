package fr.bluecode.xlient.api.model

import java.util.*

open class ImageAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val url: String,
    val preview_url: String?) : Attachment(id, last_updated, hosted_url)
