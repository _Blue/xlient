package fr.bluecode.xlient.api.model

import fr.bluecode.xlient.api.Lazy
import java.util.*

open class Contact(
    id: Int,
    last_updated: Date,
    val display_name: String,
    val display_image: String?,
    val sources: Lazy<List<SourceContact>>,
    val is_me: Boolean
): DataModel(id, last_updated)
