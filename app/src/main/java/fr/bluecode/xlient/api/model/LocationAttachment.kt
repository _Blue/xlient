package fr.bluecode.xlient.api.model

import java.util.*

open class LocationAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val latitude: Double?,
    val longitude: Double?,
    val image_url: String?,
    val map_url: String?) : Attachment(id, last_updated, hosted_url)
