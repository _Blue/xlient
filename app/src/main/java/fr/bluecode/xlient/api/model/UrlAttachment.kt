package fr.bluecode.xlient.api.model

import java.util.*

open class UrlAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val url: String,
    val title: String?,
    val description: String?,
    val original_url: String?) : Attachment(id, last_updated, hosted_url)
