package fr.bluecode.xlient.api.model

import java.io.Serializable
import java.util.*

open class DataModel(val id: Int, val last_updated: Date): Serializable