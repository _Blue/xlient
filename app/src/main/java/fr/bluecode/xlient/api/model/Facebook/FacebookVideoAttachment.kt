package fr.bluecode.xlient.api.model.Facebook

import fr.bluecode.xlient.api.model.VideoAttachment
import java.util.*

open class FacebookVideoAttachment(
    id: Int,
    last_updated: Date,
    hosted_url: String?,
    val facebook_attachment_id: String,
    url: String,
    length_ms: Int?
) : VideoAttachment(id, last_updated, hosted_url, url, length_ms)