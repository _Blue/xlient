package fr.bluecode.xlient.api.model.Synthetic

import fr.bluecode.xlient.api.model.*
import java.util.*

class SyntheticMessage(
    id: Int,
    last_updated: Date,
    text: String,
    author: Contact,
    conversation: Conversation?,
    timestamp: Date,
    edited_timestamp: Date?,
    attachments: List<Attachment>,
    reply_to: Message?,
    reactions: List<MessageReaction>,
    val remote_id: Int?,
    val sent_timestamp: Date?
): Message(id, last_updated, text, author, conversation, edited_timestamp, timestamp, attachments, reply_to, reactions)