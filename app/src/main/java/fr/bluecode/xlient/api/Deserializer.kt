package fr.bluecode.xlient.api


import android.util.Log
import androidx.core.text.isDigitsOnly
import com.fasterxml.jackson.databind.util.ISO8601DateFormat
import com.jsoniter.JsonIterator
import com.jsoniter.ValueType
import fr.bluecode.xlient.api.model.*
import fr.bluecode.xlient.api.model.Facebook.*
import fr.bluecode.xlient.api.model.Synthetic.SyntheticMessage
import fr.bluecode.xlient.api.model.Synthetic.SyntheticSource
import fr.bluecode.xlient.exceptions.SerializationException
import fr.bluecode.xlient.data.ObjectPool
import java.lang.Exception
import java.lang.RuntimeException
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodHandles
import java.lang.reflect.Field
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.declaredMembers
import kotlin.reflect.full.superclasses
import kotlin.reflect.jvm.javaConstructor

private val lookup = MethodHandles.publicLookup()

val types = listOf(
    Source::class,
    FacebookSource::class,
    Contact::class,
    SourceContact::class,
    FacebookSourceContact::class,
    Conversation::class,
    FacebookConversation::class,
    Attachment::class,
    ImageAttachment::class,
    UrlAttachment::class,
    FileAttachment::class,
    AudioAttachment::class,
    VideoAttachment::class,
    LocationAttachment::class,
    FacebookUrlAttachment::class,
    FacebookImageAttachment::class,
    FacebookLocationAttachment::class,
    FacebookAudioAttachment::class,
    FacebookVideoAttachment::class,
    FacebookFileAttachment::class,
    FacebookStickerAttachment::class,
    Message::class,
    FacebookMessage::class,
    Error::class,
    UploadedAttachment::class,
    ReadMarker::class,
    MessageReaction::class,
    FacebookMessageReaction::class,
    SyntheticMessage::class,
    SyntheticSource::class
).map { it.simpleName to loadClass(it) }.toMap()

data class TypeInformation(
    val type: KClass<*>,
    val constructor: MethodHandle,
    val arguments: Map<String, Pair<KClass<*>, Int>>,
    val fields: Map<String, Field>,
    val typeString: String,
    val types: List<String>
)

data class PostAssign(
    val path: String,
    val childId: Int
)

private fun loadClass(type: KClass<*>): TypeInformation
{
    assert(type.constructors.count() == 1)

    val ctor = type.constructors.first()

    val fields = mutableMapOf<String, Field>()
    for (e in (type.allSuperclasses + type).filter { it.qualifiedName!!.startsWith("fr.bluecode.xlient.api.model") })
    {
        for (f in e.java.declaredFields)
        {
            fields[f.name] = f
            f.isAccessible = true
        }
    }

    val typeString = getTypeString(type)
    return TypeInformation(
        type,
        lookup.unreflectConstructor(ctor.javaConstructor),
        ctor.parameters.mapIndexed{ i, it -> it.name!! to Pair(it.type.classifier as KClass<*>, i)}.toMap(),
        fields,
        typeString,
        typeString.split('/'))
}

private fun getTypeString(type: KClass<*>): String
{
    var typeIt = type
    var output = ""

    while (typeIt != DataModel::class)
    {
        output = if (output == "") typeIt.simpleName!! else "${typeIt.simpleName}/$output"

        val superclasses = typeIt.superclasses.filter { it.qualifiedName != "java.io.Serializable" }
        assert(superclasses.count() == 1)
        typeIt = superclasses.first()
    }

    return output
}

open class Deserializer(private val pool: ObjectPool, private val overwritePool: Boolean = false)
{
    private val timeFormat = ISO8601DateFormat()

    fun deserialize(
        content: ByteArray,
        readObjects: MutableSet<Int> = mutableSetOf()): Any?
    {
        val json = JsonIterator.deserialize(content)
        return deserializeJson(json, "/", readObjects)
    }

    fun deserializeJson(json: com.jsoniter.any.Any, path: String ="", readObjects: MutableSet<Int> = mutableSetOf()): Any?
    {
        val postAssign = mutableListOf<PostAssign>()

        val root = deserializeJsonImpl(json, path, readObjects, postAssign)
        for (e in postAssign)
        {
            applyPostAssign(root, e.path.split('/').filter { it.isNotBlank() }, e.childId)
        }

        return root
    }

    fun deserializeJsonImpl(json: com.jsoniter.any.Any, path: String, readObjects: MutableSet<Int>, postAssign: MutableList<PostAssign>): Any?
    {
        return if (json.valueType() == ValueType.ARRAY)
        {
            deserializeArray(json.asList(), path, readObjects, postAssign)
        }
        else
        {
            deserializeObject(json.asMap(), path, readObjects, postAssign)
        }
    }


    private fun applyPostAssign(root: Any?, path: List<String>, target: Int)
    {
        if (root == null)
        {
            throw RuntimeException("Postassign error in deserializer: Path $path couldn't be resolved")
        }

        if (path.first().isDigitsOnly()) // Array access
        {
            val index = path.first().toInt()
            assert(root is List<*>)

            applyPostAssign((root as List<*>)[index], path.drop(1), target)
            return
        }

        val field = types[root::class.simpleName]!!.fields[path.first()]!!

        if (path.size == 1) // Leaf is reached
        {
            val value = ObjectPool.get<DataModel>(target)
            if (value == null)
            {
                throw RuntimeException("Postassign error in deserializer: Object $target not found in pool")
            }

            field.set(root, ObjectPool.get(target))
        }
        else
        {
            applyPostAssign(field.get(root), path.drop(1), target)
        }
    }

    private fun deserializeArray(content: List<com.jsoniter.any.Any>, path: String, readObjects: MutableSet<Int>, postAssign: MutableList<PostAssign>) : List<Any?>
    {
        val output = ArrayList<Any?>()
        for (i in IntRange(0, content.size - 1))
        {
            output.add(deserializeObject(content[i].asMap(), "$path/$i", readObjects, postAssign))
        }

        return output
    }

    private fun deserializeObject(
        content: Map<String, com.jsoniter.any.Any>,
        path: String = "",
        readObjects: MutableSet<Int> = mutableSetOf(),
        postAssign: MutableList<PostAssign>): Any?
    {
        val cachedObject = objectPoolLookup(content["id"]!!.toInt(), readObjects)
        if (cachedObject != null)
        {
            return cachedObject
        }

        val resolvedType = resolveType(readField(content, "type", path).toString())

        return constructImpl(resolvedType, content, path, readObjects, postAssign)
    }

    private fun readField(content: Map<String, com.jsoniter.any.Any>, name: String, path: String): com.jsoniter.any.Any
    {
        return content[name] ?: throw SerializationException("Missing field '$name' in $path")
    }

    private fun objectPoolLookup(id: Int, readObjects: MutableSet<Int>): DataModel?
    {
        return if (overwritePool && !readObjects.contains(id))
        {
            null
        }
        else
        {
            pool.get(id)
        }
    }

    open fun constructImpl(
        type: TypeInformation,
        content: Map<String, com.jsoniter.any.Any>,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>): Any?
    {
        val id = content["id"]!!.toInt()

        var value = objectPoolLookup(id, readObjects)
        if (value != null)
        {
            return value
        }
        else if (readObjects.contains(id))
        {
            // Happens if are reading a forward reference to a field we are in the process of deserializing
            // ex: Conversation.last_read_message

            postAssign.add(PostAssign(path, id))
            return null
        }
        readObjects.add(id)


        val arguments = arrayOfNulls<Any?>(type.arguments.size)

        for (e in content.keys.sorted()) // Iteration order is important due to compression
        {
            val argument = type.arguments[e]
            if (argument != null)
            {
                arguments[argument.second] = buildArgument(e, argument.first, content, path, readObjects, postAssign)
            }
        }

        try
        {
            value = type.constructor.invokeWithArguments(arguments.toList()) as DataModel
        }
        catch (e: Exception)
        {
            throw RuntimeException("Failed to construct object $path", e)
        }

        pool.store(value)

        return value
    }

    private fun constructImpl(
        content: List<com.jsoniter.any.Any>,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>): Any
    {
        val output = ArrayList<Any?>()
        for (i in IntRange(0, content.size - 1))
        {
            // JVM drops template instantiation info at run time.
            // This is the only way to properly construct the inner type of a list

            val inner = content[i]
            val type = resolveType(inner["type"].toString())
            output.add(construct(type.type , inner, "$path/$i", readObjects, postAssign))
        }

        return output
    }

    private fun buildArgument(
        name: String,
        type: KClass<*>,
        content: Map<String, com.jsoniter.any.Any>,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>) : Any?
    {
        val innerPath = "$path/${name}"
        val field = readField(content, name, innerPath)
        try
        {
            return construct(
                type,
                field,
                innerPath,
                readObjects,
                postAssign
            )
        }
        catch (e: ClassCastException)
        {
            throw SerializationException("Serialization error while reading field at: $innerPath, expected type: ${type}, actual type ${field.javaClass}")
        }
    }

    private fun construct(
        type: KClass<*>,
        content: com.jsoniter.any.Any,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>): Any?
    {
        if (content.valueType() == ValueType.NULL)
        {
            return null
        }

        return when (type)
        {
            Int::class -> if (content.valueType() == ValueType.NUMBER) content.toInt() else content.asMap()["id"]!!.toInt() // Special case for reference only fields
            String::class -> content.toString()
            Float::class -> content.toFloat()
            Double::class -> content.toDouble()
            Boolean::class -> content.toBoolean()
            List::class -> constructImpl(content.asList(), path, readObjects, postAssign)
            Date::class -> timeFormat.parse(content.toString())
            Lazy::class -> constructLazy(content, path, readObjects, postAssign)
            else -> deserializeObject(content.asMap(), path, readObjects, postAssign)
        }
    }

    private fun constructLazy(
        content: com.jsoniter.any.Any,
        path: String,
        readObjects: MutableSet<Int>,
        postAssign: MutableList<PostAssign>): Lazy<*>
    {
        val lazy =  if (content.valueType() == ValueType.ARRAY)
        {
            Lazy { deserializeArray(content.asList(), path, readObjects, postAssign) as List<*> }
        }
        else
        {
            Lazy { deserializeObject(content.asMap(), path, readObjects, postAssign)!!}
        }

        if (overwritePool)
        {
            lazy.get() // Trigger lazy lookup to make sure object pool gets overwritten (also needed if object is compressed)
        }

        return lazy
    }

    private fun resolveType(typeString: String): TypeInformation
    {
        val splited = typeString.split('/')

        for (e in splited.reversed())
        {
            if (types.containsKey(e))
            {
                return types[e]!!
            }
            Log.w("Deserializer", "Unknown type: $e, moving on to base type")
        }

        throw RuntimeException("Failed to resolve type: $typeString")
    }
}