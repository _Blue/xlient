package fr.bluecode.xlient.api.model

import java.util.*

open class Attachment(
    id: Int,
    last_updated: Date,
    var hosted_url: String?
    ) : DataModel(id, last_updated)