package fr.bluecode.xlient.settings

import android.content.Context
import android.util.AttributeSet
import androidx.preference.EditTextPreference

public class IntEditTextPreference: EditTextPreference
{
    private var defaultValue: Int = 0

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        defaultValue = attrs!!.getAttributeValue("http://schemas.android.com/apk/res-auto", "defaultValue")?.toInt() ?: 0
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )
    {
        defaultValue = attrs!!.getAttributeValue("http://schemas.android.com/apk/res-auto", "defaultValue")?.toInt() ?: 0
    }

    override fun getPersistedString(defaultReturnValue: String?): String
    {
        return getPersistedInt(defaultValue).toString()
    }

    override fun persistString(value: String?): Boolean
    {
        return persistInt(Integer.valueOf(value!!))
    }
}