package fr.bluecode.xlient.settings

import android.os.Bundle
import android.os.Process.SIGNAL_KILL
import android.text.InputType
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import fr.bluecode.xlient.R
import fr.bluecode.xlient.api.Client
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.background.NotificationHandler
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.DatabaseHelper

class SettingsActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.settings,
                SettingsFragment()
            )
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    class SettingsFragment : PreferenceFragmentCompat()
    {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?)
        {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            val clearCache = findPreference<Preference>("clear_cache")!!
            clearCache.setOnPreferenceClickListener { clearCache(); true }

            findPreference<Preference>("test_notification")!!.setOnPreferenceClickListener { displayTestNotification(); true }
        }

        private fun clearCache()
        {
            val helper = DatabaseHelper
            helper.clear()

            Toast.makeText(context!!, "Object cache cleared", Toast.LENGTH_LONG).show()
            activity?.finishAndRemoveTask()
            android.os.Process.killProcess(android.os.Process.myPid());

        }

        private fun displayTestNotificationImpl(messageId: Int)
        {
            val client = Client(loadConfiguration(context!!), ObjectPool)

            client.fetchObject<Message>(
                {NotificationHandler.onNewMessageReceived(it)},
                {Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()},
                messageId
            )
        }

        private fun displayTestNotification()
        {
            val builder = AlertDialog.Builder(context!!)
            builder.setTitle("Enter message id")

            val input = EditText(context!!)
            input.inputType = InputType.TYPE_CLASS_NUMBER
            builder.setView(input)

            builder.setPositiveButton("OK") { _, _ -> displayTestNotificationImpl(input.text.toString().toInt()) }
            builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            builder.show()
        }
    }

}