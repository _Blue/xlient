package fr.bluecode.xlient.settings

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import java.io.Serializable

private const val endpointKey: String = "endpoint_url"
private const val websocketEndpointKey: String = "websocket_endpoint_url"
private const val timeoutKey: String = "timeout"
private const val cachePathKey: String = "cache_path"
private const val pollingRefreshKey: String = "polling_refresh"
private const val backgroundPollingRefreshKey: String = "background_polling_refresh"
private const val enableServiceKey: String = "enable_service"
private const val searchResultCountKey: String = "search_result_count"
private const val messagesPerRequestKey: String = "messages_per_request"
private const val enableWebsocketKey: String = "enable_websocket"
private const val maxNotificationPerConversationKey: String = "max_notification_per_conversation"
private const val maxNotificationsKey: String = "max_notifications"
private const val enableCompactSerializationKey: String = "enable_compact_serialization"
private const val conversationsToShowKey: String = "conversations_to_show"
private const val conversationsPerFetchKey: String = "conversations_per_fetch"
private const val maxParallelRequestsKey: String = "max_parallel_requests"
private const val messagesToCachePerConversationKey: String = "messages_to_cache_per_conversation"
private const val propagateReadUpdatesToBackendsKey: String = "propagate_read_markers_to_backends"
private const val startAtBootKey: String = "start_at_boot"

fun loadConfiguration(preferences: SharedPreferences): Configuration
{
    val websocketEndpoint = preferences.getString(websocketEndpointKey, null)

    return Configuration(
        preferences.getString(endpointKey, null),
        preferences.getInt(timeoutKey, 20000),
        preferences.getString(cachePathKey, null),
        preferences.getInt(pollingRefreshKey, 60),
        preferences.getInt(
            backgroundPollingRefreshKey,
            600
        ),
        preferences.getBoolean(
            enableServiceKey,
            true
        ),
        preferences.getInt(searchResultCountKey, 20),
        preferences.getInt(messagesPerRequestKey, 20),
        preferences.getBoolean(enableWebsocketKey, true),
        preferences.getInt(maxNotificationPerConversationKey, 5),
        preferences.getInt(maxNotificationsKey, 5),
        preferences.getBoolean(enableCompactSerializationKey, true),
        preferences.getInt(conversationsToShowKey, 50),
        preferences.getInt(conversationsPerFetchKey, 50),
        preferences.getInt(maxParallelRequestsKey, 5),
        preferences.getInt(messagesToCachePerConversationKey, 3),
        preferences.getBoolean(startAtBootKey, true),
        preferences.getBoolean(propagateReadUpdatesToBackendsKey, false),
        if (websocketEndpoint.isNullOrBlank()) null else websocketEndpoint
    )
}

fun loadConfiguration(context: Context): Configuration
{
    return loadConfiguration(
        PreferenceManager.getDefaultSharedPreferences(
            context
        )
    )
}

fun saveConfiguration(configuration: Configuration, preferences: SharedPreferences)
{
    val editor = preferences.edit()
    editor.putString(endpointKey, configuration.endpointUrl)
    editor.putInt(timeoutKey, configuration.timeout)
    editor.putString(cachePathKey, configuration.cachePath)
    editor.putInt(pollingRefreshKey, configuration.pollingWait)
    editor.putInt(backgroundPollingRefreshKey, configuration.backgroundPollingRefresh)
    editor.putBoolean(enableServiceKey, configuration.enableService)
    editor.putInt(searchResultCountKey, configuration.searchResultCount)
    editor.putInt(messagesPerRequestKey, configuration.messagesPerRequest)
    editor.putBoolean(messagesPerRequestKey, configuration.enableWebsocket)
    editor.putInt(maxNotificationPerConversationKey, configuration.maxNotificationPerConversation)
    editor.putInt(maxNotificationsKey, configuration.maxNotifications)
    editor.putBoolean(enableCompactSerializationKey, configuration.enableCompactSerialization)
    editor.putInt(conversationsToShowKey, configuration.conversationToShow)
    editor.putInt(conversationsPerFetchKey, configuration.conversationsPerFetch)
    editor.putInt(maxParallelRequestsKey, configuration.maxParallelRequests)
    editor.putInt(messagesToCachePerConversationKey, configuration.messagesToCachePerConversation)
    editor.putBoolean(startAtBootKey, configuration.startAtBoot)
    editor.putBoolean(propagateReadUpdatesToBackendsKey, configuration.propagateReadUpdatesToBackends)
    editor.apply()
}

class Configuration(
    var endpointUrl: String?,
    var timeout: Int,
    var cachePath: String?,
    var pollingWait: Int,
    var backgroundPollingRefresh: Int,
    var enableService: Boolean,
    var searchResultCount: Int,
    var messagesPerRequest: Int,
    var enableWebsocket: Boolean,
    var maxNotificationPerConversation: Int,
    val maxNotifications: Int,
    val enableCompactSerialization: Boolean,
    val conversationToShow: Int,
    val conversationsPerFetch: Int,
    val maxParallelRequests: Int,
    val messagesToCachePerConversation: Int,
    val startAtBoot: Boolean,
    val propagateReadUpdatesToBackends: Boolean,
    val websocketEndpoint: String?) : Serializable
{
}