package fr.bluecode.xlient.adapters

import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.api.model.UrlAttachment

class UrlMessageAdapter(
    message: Message,
    val attachment: UrlAttachment,
    author: ContactAdapter,
    firstUnread: Boolean): MessageAdapter(message, author, firstUnread)
{
    override fun getId(): String
    {
        return attachment.id.toString()
    }

    override fun getText(): String
    {
        return attachment.original_url ?: attachment.url
    }
}