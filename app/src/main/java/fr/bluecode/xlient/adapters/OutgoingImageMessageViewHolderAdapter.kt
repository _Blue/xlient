package fr.bluecode.xlient.adapters

import android.view.View
import com.stfalcon.chatkit.messages.MessageHolders
import fr.bluecode.xlient.ConversationActivity

class OutgoingImageMessageViewHolderAdapter(private val view: View?, payload: Any?) : MessageHolders.OutcomingImageMessageViewHolder<ImageMessageAdapter>(view, payload)
{
    override fun onBind(message: ImageMessageAdapter?)
    {
        super.onBind(message)

        setMessageHolderText(time, message!!.message, (payload!! as ConversationActivity).conversation)

        setUnreadMarker(message, view!!)
    }
}