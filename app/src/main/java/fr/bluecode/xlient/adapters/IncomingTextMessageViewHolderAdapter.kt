package fr.bluecode.xlient.adapters

import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.stfalcon.chatkit.messages.MessageHolders
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.Facebook.FacebookMessageReaction
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.ConversationActivity
import fr.bluecode.xlient.R


fun setMessageHolderText(time: TextView, message: Message, conversation: Conversation)
{
    appendMarkers(time, message, conversation)
    appendReactions(time, message, conversation)
}

fun appendMarkers(time: TextView, message: Message, conversation: Conversation)
{
    val markers =  conversation.read_markers.filter { it.message == message.id }
    if (markers.size == 1)
    {
        time.text = "Read by ${contactName(conversation, markers.first().contact)} ${formatTime(markers.first().timestamp)}\r\n${time.text}"
    }
    else if (markers.size > 1)
    {
        time.text = "Read by ${markers.map { contactName(conversation, it.contact) }.joinToString()}\r\n${time.text}"
    }
}

fun appendReactions(time: TextView, message: Message, conversation: Conversation)
{
    for(e in message.reactions)
    {
        val emoji = if (e is FacebookMessageReaction) e.emoji else "[Unknown ${e.javaClass.canonicalName}]"
        time.text = "${contactName(conversation, e.author)}: $emoji\r\n${time.text}"
    }
}

fun contactName(conversation: Conversation, id: Int): String
{
    val contacts =  conversation.members.get().union(conversation.gone_members.get())

    return contacts.firstOrNull { it.id == id }?.display_name ?: "[Contact $id not found]"
}

fun setUnreadMarker(message: MessageAdapter, view: View)
{
    if (message.isFirstUnread())
    {
        view.background = view.context.getDrawable(R.drawable.unread_message_box)
    }
    else
    {
        view.background = null // Required as views can be reused to hold other messages
    }
}


class IncomingTextMessageViewHolderAdapter(private val view: View?, payload: Any?) : MessageHolders.IncomingTextMessageViewHolder<MessageAdapter>(view, payload)
{
    override fun onBind(message: MessageAdapter?)
    {
        super.onBind(message)

        setMessageHolderText(time, message!!.message, (payload!! as ConversationActivity).conversation)

        setUnreadMarker(message, view!!)
    }
}