package fr.bluecode.xlient.adapters

import android.view.View
import com.stfalcon.chatkit.messages.MessageHolders
import fr.bluecode.xlient.ConversationActivity
import fr.bluecode.xlient.R
import fr.bluecode.xlient.api.model.Synthetic.SyntheticMessage

class OutgoingTextMessageViewHolderAdapter(private val view: View?, payload: Any?) : MessageHolders.OutcomingTextMessageViewHolder<MessageAdapter>(view, payload)
{
    override fun onBind(message: MessageAdapter?)
    {
        super.onBind(message)

        setMessageHolderText(time, message!!.message, (payload!! as ConversationActivity).conversation)

        setUnreadMarker(message, view!!)

        if (message.message is SyntheticMessage && (message.message as SyntheticMessage).remote_id == null)
        {
            view.background = view.context.getDrawable(R.drawable.unsent_message_box)
        }
    }
}