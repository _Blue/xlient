package fr.bluecode.xlient.adapters

import android.text.format.DateUtils
import com.stfalcon.chatkit.utils.DateFormatter
import java.util.*

fun formatTime(date: Date): String
{
    return DateUtils.getRelativeTimeSpanString(date.time, Calendar.getInstance().timeInMillis , 0L, DateUtils.FORMAT_ABBREV_ALL).toString()
}

class TimeFormatterAdapter : DateFormatter.Formatter
{
    override fun format(date: Date?): String
    {
        return formatTime(date!!)
    }
}