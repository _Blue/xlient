package fr.bluecode.xlient.adapters

import com.stfalcon.chatkit.commons.models.IUser
import fr.bluecode.xlient.api.model.Contact

class ContactAdapter (val contact: Contact): IUser
{
    override fun getAvatar(): String?
    {
        return contact.display_image
    }

    override fun getName(): String
    {
        return contact.display_name
    }

    override fun getId(): String
    {
        return contact.id.toString()
    }
}