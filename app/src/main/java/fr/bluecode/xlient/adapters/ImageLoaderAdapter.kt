package fr.bluecode.xlient.adapters

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import fr.bluecode.xlient.R

class ImageLoaderAdapter: ImageLoader
{
    override fun loadImage(imageView: ImageView?, url: String?, payload: Any?)
    {
        Picasso.get().load(url).placeholder(R.drawable.ic_loading).into(imageView)
    }
}