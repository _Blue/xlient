package fr.bluecode.xlient.adapters

import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.IUser
import fr.bluecode.xlient.api.model.Message
import java.util.*

open class MessageAdapter (open var message: Message, private val author: IUser, var firstUnread: Boolean): IMessage
{
    override fun getId(): String
    {
        return message.id.toString()
    }

    override fun getCreatedAt(): Date
    {
        return message.timestamp
    }

    override fun getUser(): IUser
    {
        return author
    }

    override fun getText(): String
    {
        return if (message.reply_to != null)
        {
            val prefix = "[Reply: ${message.reply_to!!.text.take(100)}]\n\n"
            prefix + message.text
        }
        else
        {
            message.text
        }
    }

    fun isFirstUnread(): Boolean
    {
        return firstUnread
    }
}