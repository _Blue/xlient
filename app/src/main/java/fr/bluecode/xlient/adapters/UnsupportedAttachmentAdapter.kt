package fr.bluecode.xlient.adapters

import com.stfalcon.chatkit.commons.models.IUser
import fr.bluecode.xlient.api.model.Attachment
import fr.bluecode.xlient.api.model.Message

open class UnsupportedAttachmentAdapter (
    message: Message,
    private val attachment: Attachment,
    author: IUser,
    firstUnread: Boolean): MessageAdapter(message, author, firstUnread)
{
    override fun getId(): String
    {
        return attachment.id.toString()
    }

    override fun getText(): String
    {
        return "[Unsuported attachment type: ${attachment.javaClass.simpleName}, id: ${attachment.id}]"
    }
}