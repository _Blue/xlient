package fr.bluecode.xlient.adapters

import android.text.Layout
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.core.view.children
import com.stfalcon.chatkit.commons.models.IDialog
import com.stfalcon.chatkit.dialogs.DialogsListAdapter

class ConversationHolderAdapter(itemView: View) :  DialogsListAdapter.DialogViewHolder<ConversationAdapter>(itemView)
{
    override fun onBind(dialog: ConversationAdapter)
    {
        super.onBind(dialog)

        if (dialog.conversation.last_message?.author?.is_me != true)
        {
            return;
        }

        if (dialog.lastMessage == null || !dialog.conversation.read_markers.any())
        {
            return
        }

        val read = dialog.conversation.read_markers.count { it.message == dialog.lastMessage!!.message.id } == dialog.conversation.members.get().count{ !it.is_me }

        if (!read)
        {
            return
        }

        val latestReadTs = dialog.conversation.read_markers.minBy { it.timestamp }!!.timestamp

        tvDate.text = tvDate.text.toString() + ", read ${formatTime(latestReadTs)}"
    }
}