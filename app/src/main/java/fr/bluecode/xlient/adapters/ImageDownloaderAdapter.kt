package fr.bluecode.xlient.adapters

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.squareup.picasso.Picasso
import java.io.File

class ImageDownloaderAdapter (private val onLoaded: (bitmap: Bitmap) -> Unit, private val onError: (Exception) -> Unit, private  val file: File):
    com.squareup.picasso.Target
{
    override fun onPrepareLoad(placeHolderDrawable: Drawable?)
    {
    }

    override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?)
    {
        onError(e!!)
    }

    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?)
    {
        onLoaded(bitmap!!)
    }
}