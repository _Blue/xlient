package fr.bluecode.xlient.adapters

import com.stfalcon.chatkit.commons.models.MessageContentType
import fr.bluecode.xlient.api.model.ImageAttachment
import fr.bluecode.xlient.api.model.Message

class ImageMessageAdapter(
    message: Message,
    val attachment: ImageAttachment,
    author: ContactAdapter,
    firstUnread: Boolean): MessageAdapter(message, author, firstUnread), MessageContentType.Image
{
    override fun getImageUrl(): String?
    {
        return attachment.hosted_url ?: (attachment.preview_url ?: attachment.url)
    }

    override fun getId(): String
    {
        return attachment.id.toString()
    }

    override fun getText(): String
    {
        return "[Image]"
    }
}