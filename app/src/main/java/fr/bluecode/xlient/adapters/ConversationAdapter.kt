package fr.bluecode.xlient.adapters

import com.stfalcon.chatkit.commons.models.IDialog
import com.stfalcon.chatkit.commons.models.IUser
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.Message

const val EMPTY_MESSAGE_ID = -1

class ConversationAdapter(var conversation: Conversation) : IDialog<MessageAdapter>
{
    private val members : MutableList<IUser> = conversation.members.get().map { ContactAdapter(it) }.toMutableList()

    private var messages = mutableListOf<MessageAdapter>()

    fun getMessages(): List<Message>
    {
        return messages.map { it.message }
    }

    override fun getDialogPhoto(): String?
    {
        return conversation.display_image ?: conversation.members.get().firstOrNull{ !it.is_me && it.display_image != null}?.display_image
    }

    override fun getUnreadCount(): Int
    {
        return conversation.unread_count
    }

    fun addMessages(messages: List<MessageAdapter>)
    {
        for (e in messages)
        {
            val id = this.messages.indexOfFirst { it.id == e.id }
            if (id != -1)
            {
                this.messages.removeAt(id)
            }

            this.messages.add(e)
        }

        this.messages.sortBy { it.message.timestamp }
    }

    override fun setLastMessage(message: MessageAdapter?)
    {
        assert(message != null)

        // NO-OP (call addMessages instead)
    }

    override fun getId(): String
    {
        return conversation.id.toString()
    }

    override fun getUsers(): MutableList<out IUser>
    {
        return members
    }

    override fun getLastMessage(): MessageAdapter?
    {
        // Chatkit doesn't like conversation without messages, so generate a dummy one here
        if (messages.isEmpty())
        {
            val message = Message(EMPTY_MESSAGE_ID,
                                  conversation.last_updated,
                            "[Empty]",
                                  conversation.members.get().first(),
                                  conversation,
                                  null,
                                  conversation.last_updated,
                                  listOf(),
                                  null,
                                  listOf());

            return MessageAdapter(message, members.first(), true)
        }

        return messages.lastOrNull()
    }

    override fun getDialogName(): String
    {
        return conversation.display_name
    }
}