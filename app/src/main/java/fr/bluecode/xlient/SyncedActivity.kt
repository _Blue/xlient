package fr.bluecode.xlient

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Handler
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.background.IDataModelNotificationsHandler
import fr.bluecode.xlient.background.Service
import fr.bluecode.xlient.background.ServiceBinder
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.settings.loadConfiguration
import java.lang.RuntimeException

abstract class SyncedActivity : AppCompatActivity(), IDataModelNotificationsHandler
{
    protected lateinit var config: Configuration

    private var serviceConnection: ServiceConnection? = null

    private var binder: ServiceBinder? = null

    private var active = false

    override fun isInteractive(): Boolean
    {
        return active
    }

    override fun onResume()
    {
        config = loadConfiguration(this)

        active = true
        binder?.onInteractiveStateChange()

        super.onResume()
    }

    override fun onStop()
    {
        active = false

        binder?.onInteractiveStateChange()

        super.onStop()
    }

    override fun onDestroy()
    {
        disconnectService()
        super.onDestroy()
    }

    abstract fun onObjectUpdated(dataModel: DataModel)

    abstract fun onServiceError(error: Exception)

    protected fun startService(lastMessage: Message?)
    {
        if (serviceConnection != null)
        {
            return
        }

        serviceConnection = object : ServiceConnection
        {
            override fun onServiceConnected(className: ComponentName, service: IBinder)
            {
                binder = service as ServiceBinder
                binder!!.subscribe(this@SyncedActivity)
                checkServiceState(binder as ServiceBinder)
            }

            override fun onServiceDisconnected(arg0: ComponentName)
            {
                serviceConnection = null
                binder = null
            }

            override fun onBindingDied(name: ComponentName?)
            {
                serviceConnection = null
                binder = null
            }
        }

        val intent = Intent(this, Service::class.java)
        intent.putExtra("last_message_id", lastMessage?.id)
        bindService(intent, serviceConnection!!, Context.BIND_AUTO_CREATE)

        startForegroundService(intent)
        binder?.onInteractiveStateChange()
    }

    private fun checkServiceState(binder: ServiceBinder)
    {
        when(binder.getServiceState())
        {
            Service.State.Backoff -> onServiceError(RuntimeException("Service is in backoff state"))
            Service.State.NoConnection -> onServiceError(RuntimeException("Service is in no connection state"))
        }
    }

    private fun disconnectService()
    {
        if (serviceConnection != null)
        {
            binder?.unsubscribe(this)
            unbindService(serviceConnection!!)
            serviceConnection = null
        }
    }

    override fun onNewObject(dataModel: DataModel)
    {
        Handler(mainLooper).post{ onObjectUpdated(dataModel) }
    }

    override fun onListenError(error: Exception)
    {
        Handler(mainLooper).post{ onServiceError(error) }
    }
}