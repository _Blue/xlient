package fr.bluecode.xlient

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.bluecode.xlient.api.Client
import fr.bluecode.xlient.api.model.DataModel
import fr.bluecode.xlient.exceptions.ErrorFoundOnBackendException
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.DatabaseHelper
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.settings.loadConfiguration

import kotlinx.android.synthetic.main.activity_single_error_display.*
import kotlinx.android.synthetic.main.content_single_error_display.*
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.Exception

class SingleErrorDisplayActivity : AppCompatActivity()
{
    private lateinit var error: Exception

    private lateinit var config: Configuration

    private lateinit var client: Client

    private lateinit var cache: ObjectCache

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_error_display)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        config = loadConfiguration(this)

        client =  Client(config, ObjectPool)
        cache = ObjectCache(config, DatabaseHelper, ObjectPool)

        error = intent.extras!!.get("error") as Exception

        if (error is ErrorFoundOnBackendException)
        {
            val relatedObject = ObjectPool.get<DataModel>((error as ErrorFoundOnBackendException).objectId)

            textView.text = "BackendError ID: ${(error as ErrorFoundOnBackendException).error.id}\r\nCode: ${(error as ErrorFoundOnBackendException).error.code}" +
                            "\r\nRelated object: ${relatedObject?.toString()}(${relatedObject?.id})" +
                            "\r\nDetails: ${(error as ErrorFoundOnBackendException).error.details}"
        }
        else
        {
            var writer = StringWriter()
            error.printStackTrace(PrintWriter(writer))

            textView.text = "$error\n\n$writer"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_single_error_display, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId)
        {
            R.id.action_acknowledge -> acknowledge()
            else -> super.onOptionsItemSelected(item)
        }

        return true
    }

    class AcknowledgeResponse
    {
        val affected: Int = 0
    }

    private fun onAcknowledged(error: ErrorFoundOnBackendException, response: AcknowledgeResponse)
    {
        client.fetchObject<DataModel>(
            {cache.cache(it); Toast.makeText(this, "${response.affected} errors affected", Toast.LENGTH_LONG).show()},
            {Toast.makeText(this, "An error occurred: $it", Toast.LENGTH_LONG).show()},
            error.objectId)
    }

    private fun acknowledge()
    {
        if (error !is ErrorFoundOnBackendException)
        {
            Toast.makeText(this, "Error is not a backend error", Toast.LENGTH_LONG).show()
            return
        }

        client.post<AcknowledgeResponse>(
            {onAcknowledged(error as ErrorFoundOnBackendException, it)},
            {Toast.makeText(this, "An error occurred: $it", Toast.LENGTH_LONG).show()},
            "error/${(error as ErrorFoundOnBackendException).error.id}/acknowledge",
            useDeserializer = false)
    }
}
