package fr.bluecode.xlient

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isEmpty
import fr.bluecode.xlient.api.model.Conversation
import fr.bluecode.xlient.api.model.Message
import fr.bluecode.xlient.data.APIDataView
import fr.bluecode.xlient.data.DataViewFactory
import fr.bluecode.xlient.data.ErrorPool
import fr.bluecode.xlient.data.ObjectPool
import fr.bluecode.xlient.data.cache.DatabaseHelper
import fr.bluecode.xlient.data.cache.ObjectCache
import fr.bluecode.xlient.settings.Configuration
import fr.bluecode.xlient.settings.loadConfiguration
import kotlinx.android.synthetic.main.activity_message_search.*
import kotlinx.android.synthetic.main.activity_message_search.errorLabel
import kotlinx.android.synthetic.main.activity_message_search.progressBar
import kotlinx.android.synthetic.main.activity_message_search.toolbar
import java.util.*


class MessageSearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener
{
    private var searchBar: SearchView? = null

    private val errorContext = ErrorPool.createContext()

    private lateinit var dataView: APIDataView

    private lateinit var config: Configuration

    private var endReached = false

    private var conversation: Conversation? = null

    class Adapter(context: Context, val messages: List<Message>, private val query: String)
        : ArrayAdapter<Message>(context, 0, messages)
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View
        {
            val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.search_result, parent, false)

            val item = getItem(position)!!

            val timestamp =  DateUtils.getRelativeTimeSpanString(item.timestamp.time, Calendar.getInstance().timeInMillis , 0L, DateUtils.FORMAT_ABBREV_ALL)

            val text = Html.escapeHtml(item.text).replace(query, "<b>$query</b>", ignoreCase = true)
            view.findViewById<TextView>(R.id.author).text = item.author.display_name
            view.findViewById<TextView>(R.id.timestamp).text = timestamp.toString()
            @Suppress("DEPRECATION")
            view.findViewById<TextView>(R.id.messageContent).text = Html.fromHtml(text)

            return view
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_search)
        setSupportActionBar(toolbar)

        title = "Search"

        config = loadConfiguration(this)
        dataView = DataViewFactory().createView(this, config)

        listView.setOnItemClickListener{_, _, index, _ -> onMessageClicked(index)}
        listView.setOnScrollChangeListener { _, _, _, _, _ -> onScroll()}

        errorLabel.setOnClickListener{ viewErrors() }

        if (intent.hasExtra("conversation"))
        {
            conversation = dataView.cache.lookupId(intent.getIntExtra("conversation", -1)) as Conversation
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_message_search, menu)
        searchBar = menu.findItem(R.id.searchBar).actionView as SearchView
        searchBar!!.setOnQueryTextListener(this)

        if (intent.hasExtra("query"))
        {
            searchBar!!.setQuery(intent.getStringExtra("query"), true)
        }

        return super.onCreateOptionsMenu(menu)
    }

    private fun onScroll()
    {
        if (listView.isEmpty() || endReached)
        {
            return
        }

        if (listView.lastVisiblePosition == listView.adapter.count - 1) // Bottom reached, continue search
        {
            search(searchBar!!.query.toString(), true)
        }
    }

    private fun onMessageClicked(index: Int)
    {
        val message = listView.adapter.getItem(index) as Message

        val intent = Intent(this, ConversationActivity::class.java)
        intent.putExtra("conversation", message.conversation!!.id)
        intent.putExtra("messages", arrayOf(message.id).toIntArray())

        startActivity(intent)
    }

    private fun onSearchResult(messages: List<Message>, query: String, append: Boolean)
    {
        if (messages.isEmpty())
        {
            progressBar.visibility = View.GONE

            if (!append) // in case of no-result query
            {
                listView.adapter = Adapter(this, messages, query)
            }

            endReached = true
            return
        }

        if (append)
        {
            val state = listView.onSaveInstanceState()
            listView.adapter = Adapter(this, (listView.adapter as Adapter).messages.union(messages).toList(), query)
            listView.onRestoreInstanceState(state)
        }
        else
        {
            listView.adapter = Adapter(this, messages, query)
        }

        progressBar.visibility = View.GONE
    }

    private fun onError(error: Exception)
    {
        ErrorPool.add(errorContext, error)

        errorLabel.text = "${ErrorPool.get(errorContext).count()} errors found"
        errorLabel.visibility = TextView.VISIBLE
        progressBar.visibility = View.GONE
    }

    private fun viewErrors()
    {
        val intent = Intent(this, ErrorDisplayActivity::class.java)
        intent.putExtra("context", errorContext)

        startActivity(intent)
    }

    private fun search(query: String, append: Boolean)
    {
        if (query.isBlank() || progressBar.visibility == View.VISIBLE)
        {
            return
        }

        if (!append)
        {
            endReached = false // new search, reset state
        }


        val from = if (!append) 0 else listView.adapter.count

        dataView.search(
            {onSearchResult(it, query, append)},
            {onError(it)},
            query,
            from,
            null,
            conversation = conversation,
            author = null
        )
        progressBar.visibility = View.VISIBLE
    }

    override fun onQueryTextSubmit(query: String?): Boolean
    {
        search(query!!, false)

        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean
    {
        return false
    }
}
